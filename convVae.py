import torch
import einops
from einops.layers.torch import Rearrange
import itertools
import numpy as np
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
import os
import MDAnalysis as mda
import warnings
import gc
import inspect
import curtsies.fmtfuncs as fmt
from typing import *
import copy
import pytorch_lightning as pl
from MDAnalysis.analysis import align
from MDAnalysis.coordinates.memory import MemoryReader
from MDAnalysis.analysis.base import AnalysisFromFunction
import argparse
import dataloader as dl 
from loss_utils import *
from torch import nn
from torch.nn import functional as F
from typing import List, Callable, Union, Any, TypeVar, Tuple
Tensor = TypeVar('torch.tensor')
import argparse

class VanillaVAE(torch.nn.Module):


    def __init__(self,
                 args: argparse.ArgumentParser,
                 in_channels: int=1,
                 latent_dim: int=128,
                 hidden_dims: List = None,
                 ) -> None:
        super(VanillaVAE, self).__init__()

        self.latent_dim = latent_dim

        modules = []
        if hidden_dims is None:
            hidden_dims = [32, 64, 128, 256, 512]

        # Build Encoder
        for h_dim in hidden_dims:
            modules.append(
                nn.Sequential(
                    nn.Conv2d(in_channels, out_channels=h_dim,
                              kernel_size= 3, stride= 2, padding  = 1),
                    # nn.BatchNorm2d(h_dim),
                    nn.LeakyReLU(True))
            )
            in_channels = h_dim
        self.prebegin_adaptive = torch.nn.AdaptiveAvgPool2d(64) ##REDUCE to 64 on PURPOSE! so that output before ADAPTIVE pooling becomes 64 too!

        self.encoder = nn.Sequential(*modules)
        self.fc_mu = nn.Linear(hidden_dims[-1]*4, latent_dim) #WIP
        self.fc_var = nn.Linear(hidden_dims[-1]*4, latent_dim) #WIP


        # Build Decoder
        modules = []

        self.decoder_input = nn.Linear(latent_dim, hidden_dims[-1]*4) #WIP

        hidden_dims.reverse()

        for i in range(len(hidden_dims) - 1):
            modules.append(
                nn.Sequential(
                    nn.ConvTranspose2d(hidden_dims[i],
                                       hidden_dims[i + 1],
                                       kernel_size=3,
                                       stride = 2,
                                       padding=1,
                                       output_padding=1),
                    # nn.BatchNorm2d(hidden_dims[i + 1]),
                    nn.LeakyReLU(True))
            )

        self.decoder = nn.Sequential(*modules)

        self.prefinal_adaptive = torch.nn.AdaptiveAvgPool2d( args.truncate // 2 ) if args.truncate >0 else torch.nn.AdaptiveAvgPool2d( args.unrolled_dim // 3 // 2 ) #So that after final_layer, it doubles in size!!!
        #Below convtranspose2d extends pixels by 2 in both directions!
        self.final_layer = nn.Sequential(
                            nn.ConvTranspose2d(hidden_dims[-1],
                                               hidden_dims[-1],
                                               kernel_size=3,
                                               stride=2,
                                               padding=1,
                                               output_padding=1),
                            # nn.BatchNorm2d(hidden_dims[-1]),
                            nn.LeakyReLU(True),
                            nn.Conv2d(hidden_dims[-1], out_channels=32,
                                      kernel_size= 7, padding= 3),
                            nn.LeakyReLU(True),
                            nn.Conv2d(32, out_channels= 16,
                                      kernel_size= 5, padding= 2),
                            nn.LeakyReLU(True),
                            nn.Conv2d(16, out_channels= 8,
                                      kernel_size= 3, padding= 1),
                            nn.LeakyReLU(True),
                            nn.Conv2d(8, out_channels= 4,
                                      kernel_size= 3, padding= 1),
                            nn.LeakyReLU(True),
                            nn.Conv2d(4, out_channels= 2,
                                      kernel_size= 3, padding= 1),
                            nn.LeakyReLU(True),
                            nn.Conv2d(2, out_channels= 1,
                                      kernel_size= 3, padding= 1),
                            nn.Sigmoid()
                            )

    def encode(self, input: Tensor) -> List[Tensor]:
        """
        Encodes the input by passing through the encoder network
        and returns the latent codes.
        :param input: (Tensor) Input tensor to encoder [N x C x H x W]
        :return: (Tensor) List of latent codes
        """
        input = input.contiguous()
        input = self.prebegin_adaptive(input)
        result = self.encoder(input)
        result = torch.flatten(result, start_dim=1) #WIP

        # Split the result into mu and var components
        # of the latent Gaussian distribution
        mu = self.fc_mu(result)
        log_var = self.fc_var(result)

        return [mu, log_var]

    def decode(self, z: Tensor) -> Tensor:
        """
        Maps the given latent codes
        onto the image space.
        :param z: (Tensor) [B x D]
        :return: (Tensor) [B x C x H x W]
        """
        b, d = z.size()
        result = self.decoder_input(z) #WIP
        result = result.view(b, 512, 2, 2) #WIP
        result = self.decoder(result)
        result = self.prefinal_adaptive(result)
        result = result.contiguous()
        result = self.final_layer(result)
        result = (result + result.permute(0,1,3,2).contiguous()) / 2 #Symmetric: B1LL
        result = result.contiguous()
        return result

    def reparameterize(self, mu: Tensor, logvar: Tensor) -> Tensor:
        """
        Reparameterization trick to sample from N(mu, var) from
        N(0,1).
        :param mu: (Tensor) Mean of the latent Gaussian [B x D]
        :param logvar: (Tensor) Standard deviation of the latent Gaussian [B x D]
        :return: (Tensor) [B x D]
        """
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return eps * std + mu

    def forward(self, input: Tensor, **kwargs) -> List[Tensor]:
        mu, log_var = self.encode(input)
        z = self.reparameterize(mu, log_var)
        # print(input.shape, mu.shape, self.decode(z).shape)
        return z, mu, log_var, self.decode(z)

    @staticmethod
    def losses(inputs, z, mu, logstd, recon: "x", beta, mean, std) -> dict:
        """
        Computes the VAE loss function.
        KL(N(\mu, \sigma), N(0, 1)) = \log \frac{1}{\sigma} + \frac{\sigma^2 + \mu^2}{2} - \frac{1}{2}
        :param args:
        :param kwargs:
        :return:
        """
        recons = recon
        input = inputs
        mu = mu
        log_var = logstd

        mse = torch.nn.MSELoss(reduction="none")(recons, input).mean(dim=(1,2,3)) #B
        kl = -0.5 * torch.sum(1 + log_var - mu ** 2 - log_var.exp(), dim = 1) #B

        if 0:
            unnormalize = dl.ProteinDatasetDistogram.unnormalize #static method
            original_unscaled = unnormalize(inputs, mean=mean, std=std) #BLL
            recon_unscaled = unnormalize(recon, mean=mean, std=std) #BLL

            original_unscaled = mds_torch(original_unscaled)[0]
            recon_unscaled = mds_torch(recon_unscaled)[0]
            print("MDS done!")

            X = original_unscaled #->B3L
            Y = recon_unscaled #->B3L
            X, Y = kabsch_torch(X, Y) #-> (B,3,L) alignment: translated and rotated to fit!
            print("Kabsch done!")
            
            rmsd = rmsd_torch(X, Y) # -> (B,)
            gdt = gdt_torch(X, Y, torch.arange(0.5, 10.5, 0.5).to(X)) # -> (B,)
            tm = tmscore_torch(X, Y) # -> (B,)

        rmsd = kl.new_zeros(kl.size())
        gdt = kl.new_zeros(kl.size())
        tm = kl.new_zeros(kl.size())

        assert mse.size(0) == kl.size(0) and mse.ndim == kl.ndim and mse.ndim == 1, "all criteria for shape must match"
        assert mse.size(0) == rmsd.size(0) and mse.size(0) == gdt.size(0) and mse.size(0) == tm.size(0), "all criteria for shape must match"
        return mse, kl, rmsd, gdt, tm
        
    def sample(self,
               num_samples:int,
               current_device: int, **kwargs) -> Tensor:
        """
        Samples from the latent space and return the corresponding
        image space map.
        :param num_samples: (Int) Number of samples
        :param current_device: (Int) Device to run the model
        :return: (Tensor)
        """
        z = torch.randn(num_samples,
                        self.latent_dim)

        z = z.to(current_device)

        samples = self.decode(z)
        return samples

    def generate(self, x: Tensor, **kwargs) -> Tensor:
        """
        Given an input image x, returns the reconstructed image
        :param x: (Tensor) [B x C x H x W]
        :return: (Tensor) [B x C x H x W]
        """

        return self.forward(x)[0]
