import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder
import collections
import argparse
import os
import wandb
import io, PIL

# d=np.load('sample_output/analyze_saved_dssp.npz')
#['sert_oc_test', 'sert_oc_recon', 'sert_ofif_test', 'sert_ofif_recon']
# input: ['test_0', 'recon_0', 'test_1', 'recon_1', ......'test_n', 'recon_n']
# output figure format
#test_0 recon_0 diff0 
#test_1 recon_1 diff1  
#....
#test_n recon_n diffn  

def plot_dssp(args: argparse.ArgumentParser, d: dict) -> None : 
    #print(d)
    keys = list(d.keys())
    n_pairs = int(len(keys)/2)
    
    for i in range(0, n_pairs):
        keys.insert(2+3*i, "diff"+str(i))


    print(keys)
    #fig, axes = plt.subplots(2, 3, figsize=(10,10))
    fig, axes = plt.subplots(n_pairs, 3, figsize=(10,5*n_pairs))
    first_two = []
    cmap = plt.get_cmap("Greys")

    for i, key in enumerate(keys):

        if i % 3 != 2:
            le = LabelEncoder() #.fit_transform()
            data = d[key]
            T, feat = data.shape
            d_num = le.fit_transform(data.reshape(-1, )).reshape(T,feat)
            axes.flatten()[i].imshow(d_num.T, )
            first_two.append(d_num)
        else:
            # axes.flatten()[i].set_title("diff")
            first: np.array = first_two[0]
            second: np.array = first_two[1]
            masked = np.ma.masked_where(np.abs(first-second) == 0, np.abs(first-second))
            print(np.unique(masked.mask))
            axes.flatten()[i].imshow(masked.mask.T, cmap=cmap)
            first_two.clear()
        axes.flatten()[i].set_xlabel("Frames")
        axes.flatten()[i].set_ylabel("Residues")
        axes.flatten()[i].set_aspect('auto')
        axes.flatten()[i].set_title(key)
        
    plt.legend()
    plt.savefig(f"sample_output/analyze_saved_dssp_{args.which_model}.png")
    
    buf = io.BytesIO()
    fig.savefig(buf)
    buf.seek(0)
    img = PIL.Image.open(buf)
    if not args.nolog:
        wimg = wandb.Image(img)
        wandb.log({"Plots": wimg})

    plt.close()


def plot_pca_64(args: argparse.ArgumentParser, pca_train, pca_val) -> None : 
    EVERYONCE = 1
    fig, axes = plt.subplots(9, 10, figsize=(10*5,5*4*5))

    for i in range(len(pca_train)):
        if i%EVERYONCE == 0:
            d1 = pca_train[i].reshape(8,8)
            axes.flatten()[i].imshow(d1)

    buf = io.BytesIO()
    fig.savefig(buf)
    buf.seek(0)
    img = PIL.Image.open(buf)
    if not args.nolog:
        wimg = wandb.Image(img)
        wandb.log({"Plots": wimg})    
    pass