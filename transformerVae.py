import torch
import einops
from einops.layers.torch import Rearrange
import itertools
import numpy as np
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
import os
import MDAnalysis as mda
import warnings
import gc
import inspect
import curtsies.fmtfuncs as fmt
from typing import *
import copy
import pytorch_lightning as pl
from MDAnalysis.analysis import align
from MDAnalysis.coordinates.memory import MemoryReader
from MDAnalysis.analysis.base import AnalysisFromFunction
import argparse
import dataloader as dl 
from loss_utils import *
from torch import nn
from torch.nn import functional as F
from typing import List, Callable, Union, Any, TypeVar, Tuple
Tensor = TypeVar('torch.tensor')
import argparse

#[Ideally] Swin transformers: For best performance by windowing!
#[In practice] Segmenter: Transformer for Semantic Segmentation: Patch, Distance class mask -> Upsample -> Distogram class map
#[Goal] DeepDist: real-value inter-residue distance prediction with deep residual convolutional network: Distance map and Class map
#Use Segmenter and DeepDist together: i.e. transformer patching, masking and upsampling for classes, and convert to distances

#Segmenter: https://github.com/rstrudel/segmenter/blob/20d1bfad354165ee45c3f65972a4d9c131f58d53/segm/model/segmenter.py#L9
#DeepDist: https://github.com/multicom-toolbox/deepdist/blob/master/lib/mulclass2realdist.py
