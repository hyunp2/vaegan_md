import torch, torchani
import transformers as tfm
import dataloader as dl 
import pytorch_lightning as pl
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import mdtraj
import MDAnalysis as mda
from MDAnalysis.analysis.base import AnalysisBase
from MDAnalysis.core.universe import Universe
from MDAnalysis.core.groups import AtomGroup
import tqdm
import pickle, json, os, copy
import gc
import collections
import networkx as nx
import yaml
import logging
import warnings
import wandb
import argparse
from typing import *
from curtsies import fmtfuncs as cf
import io, PIL
from plotly.subplots import make_subplots
import plotly.graph_objs as go
from dataloader import alignTrajectory
from MDAnalysis.analysis.distances import dist as mdadist
from joblib import Parallel, delayed
import multiprocessing
from MDAnalysis.analysis.align import _fit_to
# from MDAnalysis.transformations.fit import fit_rot_trans
from MDAnalysis.lib.log import ProgressBar
from MDAnalysis.transformations.base import TransformationBase
from MDAnalysis.analysis import align
from MDAnalysis.lib.transformations import euler_from_matrix, euler_matrix
from MDAnalysis.coordinates.base import Timestep, ProtoReader
import logging
import ast
from analysis import *

if __name__ == "__main__":
    from main import get_args
    import time
    args = get_args()

    print(cf.on_green("WARNING: This code is HARDCODED for SERT data..."))

    ###########################
    ########MDANALYSIS#########
    ###########################

    """
    #######BELOW: Preprocessing (MDAnalysis)#######
    # prot_traj, prot_ref, atom_selection = alignTrajectory(args)
    # prot_trajOF, prot_trajIF, prot_trajOC = 1
    
    args.trajectory_files = ["backbone_OF.dcd"]
    prot_trajOF_, prot_ref, atom_selectionOF = alignTrajectory(args)
    prot_trajOF_ = prot_trajOF_.copy()
    args.trajectory_files = ["backbone_IF.dcd"]
    prot_trajIF_, prot_ref, atom_selectionIF = alignTrajectory(args)
    prot_trajIF_ = prot_trajIF_.copy()
    args.trajectory_files = ["backbone_OC.dcd"]
    prot_trajOC_, prot_ref, atom_selectionOC = alignTrajectory(args)
    prot_trajOC_ = prot_trajOC_.copy()
    args.trajectory_files = ["SerT_sampleOFIF_pca_lerps.dcd"]
    prot_trajIntPCA_, prot_ref, atom_selectionInt = alignTrajectory(args)
    prot_trajIntPCA_ = prot_trajIntPCA_.copy()
    args.trajectory_files = ["SerT_sampleOFIF_lerps.dcd"]
    prot_trajIntVAE_, prot_ref, atom_selectionInt = alignTrajectory(args)
    prot_trajIntVAE_ = prot_trajIntVAE_.copy()

    atom_selection = atom_selectionOF
    align_selection = args.align_selection

    EC_dist_indices = [[449, 149], [449, 169], [449, 721], [449, 901], [449, 1017], [449, 1249], 
                        [449, 1389], [449, 1625], [449, 1649], [449, 1929], [449, 2001]]
    IC_dist_indices = [[345, 45], [345, 277], [345, 773], [345, 833], [345, 1101], [345, 1137], 
                        [345, 1517], [345, 1553], [345, 1765], [345, 1845], [345, 2101]]
    dist_indices = [[165, 901], [721, 901], [721, 2001], [165, 2001], [833, 1553], [833, 1845], 
                        [2101, 1553], [2101, 1845], [121, 1677], [409, 1045]]
    angle_indices = [[45,149], [169,277], [345,449], [721,773], [833,901], [1017,1101], 
                        [1137,1249], [1389,1517], [1553,1625], [1649,1765], [1845,1929], [2001,2101]]

    class Results(dict):
        def __setattr__(self, key, val):
            self[key] = val
        def __getattr__(self, key):
            return self[key]
        def __del__(self, key):
            del self[key]
    
    #######BELOW: Distance Analysis########
    # savez vs savez_compressed https://stackoverflow.com/questions/54238670/what-is-the-advantage-of-saving-npz-files-instead-of-npy-in-python-regard#:~:text=savez%20-->%20Save%20several%20arrays,a%20single%20file%20in%20compressed%20.
    prot_trajOF = prot_trajOF_.copy()
    prot_trajIF = prot_trajIF_.copy()
    prot_trajOC = prot_trajOC_.copy()
    
    if not os.path.isfile("SERT_distance_real_data.npz"):
        results = Results()
        NUM_FRAMES_INT = min([len(universe.trajectory) for universe in [prot_trajOF, prot_trajIF, prot_trajOC]]) #get shortest frame num and set stop value for trajectory
       
        ####Distances of REAL dcds!
        for prefix, atom_pair_indices in zip(["EC", "IC", "REST"], [EC_dist_indices, IC_dist_indices, dist_indices]):
            func = DistanceCalculations(prot_trajOF.select_atoms(atom_selection), atom_pair_indices)
            res = func.run(stop=NUM_FRAMES_INT)
            distOF = res.results["distances"] #Shortest traj (OC) = 34000 (stop argument!)
            # dispOF = res.results["displacement"] #Shortest traj (OC)
            setattr(results, f"{prefix}_OF", distOF)
            func = DistanceCalculations(prot_trajIF.select_atoms(atom_selection), atom_pair_indices)
            res = func.run(stop=NUM_FRAMES_INT)
            distIF = res.results["distances"]    
            # dispIF = res.results["displacement"] #Shortest traj (OC)
            setattr(results, f"{prefix}_IF", distIF)
            func = DistanceCalculations(prot_trajOC.select_atoms(atom_selection), atom_pair_indices)
            res = func.run(stop=NUM_FRAMES_INT)
            distOC = res.results["distances"]
            # dispOC = res.results["displacement"] #Shortest traj (OC)
            setattr(results, f"{prefix}_OC", distOC)
        np.savez_compressed("SERT_distance_real_data.npz", **results)
    else:
        data = np.load("SERT_distance_real_data.npz")
        keys = list(data)
        # distOF, distIF, distOC, dispOF, dispIF, dispOC = data["distOF"], data["distIF"], data["distOC"], data["dispOF"], data["dispIF"], data["dispOC"]
        
    
    #######BELOW: Angle Analysis#######
    # prot_traj, prot_ref, atom_selection = alignTrajectory(args)
    # prot_traj2 = prot_traj.copy() #copy.deepcopy(prot_traj)
    prot_trajOF = prot_trajOF_.copy()
    prot_trajIF = prot_trajIF_.copy()
    prot_trajOC = prot_trajOC_.copy()

    from MDAnalysis.transformations.translate import translate
    from MDAnalysis.transformations.rotate import rotateby
    # prot_traj2.trajectory.add_transformations(*[translate([100,100,100]), rotateby(angle=45, direction=[1,2,3], ag=prot_traj2.atoms)])
    if not os.path.isfile("SERT_angle_real_data.npz"):
        results = Results()
        NUM_FRAMES_INT = min([len(universe.trajectory) for universe in [prot_trajOF, prot_trajIF, prot_trajOC]]) #get shortest frame num and set stop value for trajectory

        func = AngleCalculations(prot_trajOF.select_atoms(align_selection), prot_trajOC.select_atoms(align_selection), angle_indices, radian=False)
        angOFOC = func.run(stop=NUM_FRAMES_INT).results["angles"]
        setattr(results, f"OFOC", angOFOC)
        func = AngleCalculations(prot_trajIF.select_atoms(align_selection), prot_trajOC.select_atoms(align_selection), angle_indices, radian=False)
        angIFOC = func.run(stop=NUM_FRAMES_INT).results["angles"]
        setattr(results, f"IFOC", angIFOC)
        np.savez_compressed("SERT_angle_real_data.npz", **results)
    else:
        data = np.load("SERT_angle_real_data.npz")
        keys = list(data)    
    

    #######BELOW: Interpolation Distance Analysis########
    prot_trajOF = prot_trajOF_.copy()
    prot_trajIF = prot_trajIF_.copy()
    prot_trajIntPCA = prot_trajIntPCA_.copy()
    prot_trajIntVAE = prot_trajIntVAE_.copy()

    print(cf.blue("Interpolation will sweep through the OF-OC-IF space for distance..."))
    if not os.path.isfile("SERT_distance_lerps_data.npz"):
        results = Results()
        NUM_FRAMES_INT = min([len(universe.trajectory) for universe in [prot_trajOF, prot_trajIF, prot_trajIntPCA, prot_trajIntVAE]]) #get shortest frame num and set stop value for trajectory
        # del prot_trajOF.trajectory.transformations #property;; ProtoReader does not define __del__ ... so it fails...
        # del prot_trajIF.trajectory.transformations #property
        
        ####Distances of REAL dcds!
        for prefix, atom_pair_indices in zip(["EC", "IC", "REST"], [EC_dist_indices, IC_dist_indices, dist_indices]):
            func = DistanceCalculations(prot_trajIntPCA.select_atoms(atom_selection), atom_pair_indices)
            res = func.run(stop=NUM_FRAMES_INT)
            distPCA = res.results["distances"] #Shortest traj (OC) = 34000 (stop argument!)
            # dispOF = res.results["displacement"] #Shortest traj (OC)
            setattr(results, f"{prefix}_PCA", distPCA)
            func = DistanceCalculations(prot_trajIntVAE.select_atoms(atom_selection), atom_pair_indices)
            res = func.run(stop=NUM_FRAMES_INT)
            distVAE = res.results["distances"]    
            # dispIF = res.results["displacement"] #Shortest traj (OC)
            setattr(results, f"{prefix}_VAE", distVAE)
        np.savez_compressed("SERT_distance_lerps_data.npz", **results)
    else:
        data = np.load("SERT_distance_lerps_data.npz")
        keys = list(data)
    

    #######BELOW: Interpolation Angle Analysis#######
    prot_trajOF = prot_trajOF_.copy()
    prot_trajIF = prot_trajIF_.copy()
    prot_trajIntPCA = prot_trajIntPCA_.copy()
    prot_trajIntVAE = prot_trajIntVAE_.copy()

    # BELOW: transformations must be add all at ONCE!
    # https://docs.mdanalysis.org/2.2.0/_modules/MDAnalysis/coordinates/base.html#Timestep:~:text=try%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20self.transformations%20%3D%20transformations%0A%20%20%20%20%20%20%20%20except%20ValueError%3A%0A%20%20%20%20%20%20%20%20%20%20%20%20errmsg%20%3D%20(%22Can%27t%20add%20transformations%20again.%20Please%20create%20a%20new%20%22%0A%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%22Universe%20object%22)%0A%20%20%20%20%20%20%20%20%20%20%20%20raise%20ValueError(errmsg)%20from%20None
    print(cf.blue("Interpolation will sweep through the OF-OC-IF space for angle..."))
    from MDAnalysis.transformations.translate import translate
    from MDAnalysis.transformations.rotate import rotateby
    # prot_traj2.trajectory.add_transformations(*[translate([100,100,100]), rotateby(angle=45, direction=[1,2,3], ag=prot_traj2.atoms)])
    if not os.path.isfile("SERT_angle_lerps_data.npz"):
        results = Results()
        NUM_FRAMES_INT = min([len(universe.trajectory) for universe in [prot_trajOF, prot_trajIF, prot_trajIntPCA, prot_trajIntVAE]]) #get shortest frame num and set stop value for trajectory
        # del prot_trajOF.trajectory.transformations #property
        # del prot_trajIF.trajectory.transformations #property
        func = AngleCalculations(prot_trajOF.select_atoms(align_selection), prot_trajIntPCA.select_atoms(align_selection), angle_indices, radian=False)
        angOFOC = func.run(stop=NUM_FRAMES_INT).results["angles"]
        setattr(results, f"OFOC_PCA", angOFOC)
        func = AngleCalculations(prot_trajIF.select_atoms(align_selection), prot_trajIntPCA.select_atoms(align_selection), angle_indices, radian=False)
        angIFOC = func.run(stop=NUM_FRAMES_INT).results["angles"]
        setattr(results, f"IFOC_PCA", angIFOC)
        # del prot_trajOF.trajectory.transformations #property
        # del prot_trajIF.trajectory.transformations #property
        prot_trajOF = prot_trajOF_.copy()
        prot_trajIF = prot_trajIF_.copy()
        prot_trajIntPCA = prot_trajIntPCA_.copy()
        prot_trajIntVAE = prot_trajIntVAE_.copy()
        func = AngleCalculations(prot_trajOF.select_atoms(align_selection), prot_trajIntVAE.select_atoms(align_selection), angle_indices, radian=False, add_transformations=False)
        angOFOC = func.run(stop=NUM_FRAMES_INT).results["angles"]
        setattr(results, f"OFOC_VAE", angOFOC)
        func = AngleCalculations(prot_trajIF.select_atoms(align_selection), prot_trajIntVAE.select_atoms(align_selection), angle_indices, radian=False, add_transformations=False)
        angIFOC = func.run(stop=NUM_FRAMES_INT).results["angles"]
        setattr(results, f"IFOC_VAE", angIFOC)
        np.savez_compressed("SERT_angle_lerps_data.npz", **results)
    else:
        data = np.load("SERT_angle_lerps_data.npz")
        keys = list(data)    
    
    ###########################
    ##########MDTRAJ###########
    ###########################

    #######BELOW: Preprocessing (MDTraj)#######
    args.analysis_backend = "mdtraj"
    args.atom_selection = "(resid 100 103 104 105 175 561 563 579) or (resid 327-338) or (resid 490-503) or (resid 549-557)"
    prot_traj, prot_ref, atom_selection, *_ = preprocess_trajectories(args) #Boxdim updated!

    #######BELOW: SASA Analysis#######
    sasa = SASACalculations(prot_traj)
    print(sasa)
    """

    ###########################
    ##########PLOTTING#########
    ###########################

    # import plotly.graph_objects as go
    # fig = go.Figure()
    # s=go.Scatter(
    #             x=[1, 2, 3, 4],
    #             y=[2, 1, 3, 4],
    #             error_y=dict(
    #                 type='data', # value of error bar given in data coordinates
    #                 array=[1, 2, 3],
    #                 visible=True)
    #         )
    # fig.add_trace(s)
    # fig.write_image("error_subplots.png")
    # # fig.show()

    def raw_data_stats():
        """Used for extracting mean and std from trajectories"""
        files = ["SERT_distance_real_data", "SERT_angle_real_data", "SERT_distance_lerps_data", "SERT_angle_lerps_data"] #9, 2, 6, 4
        mpl.rcParams.update(mpl.rcParamsDefault)
        mpl.rcParams['xtick.labelsize'] = 7
        mpl.rcParams['axes.labelsize'] = 9
        panels = [9, 2, 6, 4]
        for p, f in zip(panels, files):
            data = np.load(os.path.join("analysis_files", f+".npz"))
            keys = list(data)

            if int(p) == 9:
                fig, ax = plt.subplots(3, 3, sharex=False, figsize=(15,15))
            elif int(p) == 2:
                fig, ax = plt.subplots(2, figsize=(15,15))
            elif int(p) == 6:
                fig, ax = plt.subplots(3, 2, figsize=(15,15))
            elif int(p) == 4:
                fig, ax = plt.subplots(2, 2, figsize=(15,15))

            plt.suptitle(f)
            for idx, k in enumerate(keys):
                mean = data[k].mean(axis=0) #data: (nframes, feats) -> (feats, )
                std = data[k].std(axis=0)
                pct_bot = mean - np.percentile(data[k], q=0.5, axis=0) #->(feats, )
                pct_top = np.percentile(data[k], q=99.5, axis=0) - mean #->(feats, )
                ax.flatten()[idx].scatter(np.arange(mean.size), mean, c="k", )
                ax.flatten()[idx].errorbar(np.arange(std.size), mean, yerr=(pct_bot, pct_top), fmt="", c="k")
                # if k == "OFOC": print(mean, pct_bot, pct_top)
                # if k == "REST_OC": print(mean, pct_bot, pct_top)

                if k == "OFOC":
                    ax.flatten()[idx].scatter(np.arange(std.size), [5, 2.5, 0.5, 5, 3.75, 2.5, 1.75, 0.5, 2.5, 2, 2.5, 3.75], c="r", marker="x") #real angles
                elif k == "IFOC":
                    ax.flatten()[idx].scatter(np.arange(std.size), [30, 7.5, 0.5, 3.75, 7.5, 5, 5, 2.5, 2, 2, 2, 5], c="r", marker="x") #real angles
                elif k == "REST_OF":
                    ax.flatten()[idx].scatter(np.arange(std.size), [26.5, 36.3, 27.9, 34.8, 23.2, 36.4, 30.7, 39.9, 12, 13.6, ], c="r", marker="x") #Rest (EC.IC) (OF.OC.IF) distances
                elif k == "REST_OC":
                    ax.flatten()[idx].scatter(np.arange(std.size - 2), [28.1, 36.7, 27.9, 33.9, 24.5, 35.4, 31.7, 42.1], c="r", marker="x")
                elif k == "REST_IF":
                    ax.flatten()[idx].scatter(np.arange(std.size), [25.1, 37, 28.4, 27.7, 29.4, 36.1, 30.5, 41.1, 9.7, 13.5], c="r", marker="x")

                if p in [9, 6]:
                    if not "REST" in k: #REST has 10 features, others have 11 for distances
                        # with mpl.rc_context({'xtick.labelsize' : 4}):
                        ax.flatten()[idx].set_xticks(np.arange(std.size), labels=[f'TM{i}' for i in [1,2,4,5,6,7,8,9,10,11,12]])
                    else:
                        # with mpl.rc_context({'xtick.labelsize' : 4}):
                        ax.flatten()[idx].set_xticks(np.arange(std.size), labels=[f'Idx{i}' for i in [1,2,3,4,5,6,7,8,9,10]])
                    ax.flatten()[idx].set_xlabel('TMs or Index')
                    # if (p == 9 and idx == 3) or (p == 6 and idx == 2):
                    ax.flatten()[idx].set_ylabel(r'Helical displacement $\AA$')
                else:
                    #angles must have 12 features
                    ax.flatten()[idx].set_xticks(np.arange(std.size), labels=[f'TM{i}' for i in [1,2,3,4,5,6,7,8,9,10,11,12]])
                    ax.flatten()[idx].set_xlabel('TMs')
                    ax.flatten()[idx].set_ylabel(r'Angular change $\degree$') 

                ax.flatten()[idx].set_title(k)
            fig.tight_layout()
            fig.savefig(os.path.join("analysis_figures", f+".png"))
            plt.close()

        mpl.rcParams.update(mpl.rcParamsDefault)

    raw_data_stats()

    def compare_data_stats(only_following_angle_indices: np.ndarray = None):
        """Used for computing data and comparing to SI in the paper"""
        files = ["SERT_distance_real_data", "SERT_distance_lerps_data", "SERT_angle_lerps_data"] #9, 2, 6, 4
        mpl.rcParams.update(mpl.rcParamsDefault)
        mpl.rcParams['xtick.labelsize'] = 7
        mpl.rcParams['axes.labelsize'] = 7
        mpl.rcParams['legend.fontsize'] = 'small'

        panels = [9, 6, 4]
        for p, f in zip(panels, files):
            data = np.load(os.path.join("analysis_files", f+".npz"))
            keys = list(data)

            if int(p) == 9:
                # keys_ = np.array(keys).reshape(3,3)
                # bools = np.isin(keys_, ["EC_OF", "EC_IF", "EC_OC", "IC_OF", "IC_IF", "IC_OC"])
                # keys_[bools].reshape(2,3) #EC first row; IC second row
                shape = data["EC_OF"].shape #to get a shape
                keys = ["EC_OF", "EC_IF", "EC_OC", "IC_OF", "IC_IF", "IC_OC"]
                data_ = np.stack([data[k] for k in keys], axis=0).reshape(2, 3, *shape) # (2, 3, numframes, feats)
                diffs = np.concatenate((data_[:,0:1,...] - data_[:,2:3,...], data_[:,2:3,...] - data_[:,1:2,...]), axis=1) #(2,2,nf,feats)
                data = diffs.copy().reshape(-1, *shape) #(4,nf,feats) 
                keys = ["EC_OFOC", "EC_OCIF", "IC_OFOC", "IC_OCIF"]
                fig, ax = plt.subplots(2, 2, sharex=False, figsize=(15,15))
                data_means = data_.mean(axis=2, keepdims=True).copy() #(2,3,1,feats)
            elif int(p) == 6:
                shape = data["EC_PCA"].shape #to get a shape
                keys = ["EC_PCA", "EC_VAE", "IC_PCA", "IC_VAE"]
                data_ = np.stack([data[k] for k in keys], axis=0).reshape(2, 2, *shape) # (2, 2, numframes, feats)
                diffs = np.concatenate((data_[:,0:1,...] - data_means[:,2:3,...], data_[:,1:2,...] - data_means[:,2:3,...]), axis=1) #for either PCA/VAE: (2,1,nf,feats) - (2,1,1,feats) = (2,1,nf,feats); then concat -> (2,2,nf,feats)
                keys = ["EC_PCAOC", "EC_VAEOC", "IC_PCAOC", "IC_VAEOC"]
                rest_keys = ["REST_PCA", "REST_VAE"]
                data_ = np.stack([data[k] for k in rest_keys], axis=0).reshape(2, shape[0], 10) # (2, numframes, 10)
                rest_data = data_.copy() #(2,2,nf,feats) -> (2,nf,10)
                keys += rest_keys
                data = diffs.copy().reshape(-1, *shape) #(2,2,nf,feats) -> (4,nf,feats)
                fig, ax = plt.subplots(3, 2, figsize=(15,15))
            elif int(p) == 4:
                shape = data["OFOC_PCA"].shape #to get a shape
                keys = ["OFOC_PCA", "OFOC_VAE", "IFOC_PCA", "IFOC_VAE"]
                data_ = np.stack([data[k] for k in keys], axis=0).reshape(2, 2, *shape) # (2, 2, numframes, feats)
                data = data_.copy().reshape(-1, *shape) #(2,2,nf,feats) -> (4,nf,feats)
                fig, ax = plt.subplots(2, 2, figsize=(15,15))

            plt.suptitle(f)
            for idx, k in enumerate(keys):
                if p in [9]:
                    # print(data.shape)
                    mean = data[idx].mean(axis=0) #data: (4, nframes, feats) -> (feats, )
                    std = data[idx].std(axis=0) #(4, nframes, feats) -> (feats, )
                    pct_bot = mean - np.percentile(data[idx], q=0.5, axis=0) #->(feats, )
                    pct_top = np.percentile(data[idx], q=99.5, axis=0) - mean #->(feats, )
                    # if k == "EC_OCIF": print(mean, pct_bot, pct_top)
                    ax.flatten()[idx].scatter(np.arange(mean.size), mean, c="k", )
                    ax.flatten()[idx].errorbar(np.arange(std.size), mean, yerr=(pct_bot, pct_top), fmt="", c="k")
                    if k == "EC_OFOC":
                        ax.flatten()[idx].scatter(np.arange(std.size), [1.5, 0.5, 0.5, 1.5, 2, 1.5, 1.5, 0.5, 1, 0.9, 1.2], c="r", marker="x")
                    elif k == "EC_OCIF":
                        ax.flatten()[idx].scatter(np.arange(std.size), [5, 3, -1, 0.8, 3, 1, 0.8, 0.1, 0.2, -1, 0.5], c="r", marker="x")
                    elif k == "IC_OFOC":
                        ax.flatten()[idx].scatter(np.arange(std.size), [1, 0.5, 1, 0.9, 1, 0.5, 1, 1, 0.5, 0.5, 1.2], c="r", marker="x")
                    elif k == "IC_OCIF":
                        ax.flatten()[idx].scatter(np.arange(std.size), [-9, -0.1, 1, -2, 0.5, 0.1, 1, 0.5, 0.8, 0.8, -0.5], c="r", marker="x")
                    ax.flatten()[idx].set_title(k)
                else:
                    timeseries = data[idx] if not k in ["REST_PCA", "REST_VAE"] else rest_data[idx-4] #indexing gives (nf,feats)
                    # ax.flatten()[idx].plot(timeseries, label=list(map(lambda inp: f"TM{inp}", (1+np.arange(timeseries.shape[-1])).tolist() )) ) #feats number of lines with nf number x-axis
                    if p == 4:
                        #TM1 to TM12 for angles!
                        #COLOR cycles back after 10 labels!
                        if type(only_following_angle_indices) is type(None):
                            ax.flatten()[idx].plot(timeseries[:,:10], label=list(map(lambda inp: f"TM{inp}", (1+np.arange(timeseries.shape[-1])).tolist()[:10] )), ) #feats number of lines with nf number x-axis
                            ax.flatten()[idx].plot(timeseries[:,10:len(timeseries)], label=list(map(lambda inp: f"TM{inp}", (1+np.arange(timeseries.shape[-1])).tolist()[10:None] )), linestyle = 'dashdot' ) #feats number of lines with nf number x-axis
                        else:
                            print("We are here for select!!")
                            if idx < 2:
                                print(only_following_angle_indices)
                                ax.flatten()[idx].plot(timeseries[:,only_following_angle_indices], label=list(map(lambda inp: f"TM{inp}", only_following_angle_indices+1 )), ) #feats number of lines with nf number x-axis
                            else:
                                print("We are here for hist!!")
                                #If only_following_angle_indices is used, replace useless IFOC_PCA and IFOC_VAE panels with histogram
                                # ax.flatten()[idx].plot(timeseries[:,only_following_angle_indices], label=list(map(lambda inp: f"TM{inp}", only_following_angle_indices+1 )), ) #feats number of lines with nf number x-axis
                                ax.flatten()[idx].hist(timeseries[:,0], label=list(map(lambda inp: f"TM{inp}", [1] )), bins=10) #feats number of lines with nf number x-axis
                    elif p == 6:
                        #TM1 to TM12 except for TM3 for distances!!
                        if not k in ["REST_PCA", "REST_VAE"]:
                            #If TM distances, there are 11 TMs to compare!
                            tm_nums = (1+np.arange(12)).tolist()
                            tm_nums.remove(3)
                            ax.flatten()[idx].plot(timeseries[:,:10], label=list(map(lambda inp: f"TM{inp}", tm_nums[:10])),  ) #feats number of lines with nf number x-axis
                            # print(tm_nums[10:None])
                            ax.flatten()[idx].plot(timeseries[:,10:len(timeseries)], label=list(map(lambda inp: f"TM{inp}", tm_nums[10:None])), linestyle = 'dashdot' ) #feats number of lines with nf number x-axis
                        else:
                            #REST has 10 pairs!
                            ax.flatten()[idx].plot(timeseries, label=list(map(lambda inp: f"Idx{inp}", 1+np.arange(10) ))) #feats number of lines with nf number x-axis
                    if idx==1: 
                        ax.flatten()[idx].legend(bbox_to_anchor=(1, 1))
                    elif (idx==5) and (p==6):
                        #idx 5 exists only for timeseries of lerps distances (i.e. p=6)!
                        ax.flatten()[idx].legend(bbox_to_anchor=(1, 1))
                    ax.flatten()[idx].set_title(k)

                if p in [9]:
                    if not "REST" in k: #REST has 10 features, others have 11 for distances
                        # with mpl.rc_context({'xtick.labelsize' : 4}):
                        ax.flatten()[idx].set_xticks(np.arange(shape[-1]), labels=[f'TM{i}' for i in [1,2,4,5,6,7,8,9,10,11,12]])
                    else:
                        # with mpl.rc_context({'xtick.labelsize' : 4}):
                        ax.flatten()[idx].set_xticks(np.arange(shape[-1]), labels=[f'Idx{i}' for i in [1,2,3,4,5,6,7,8,9,10]])
                    ax.flatten()[idx].set_xlabel('TMs or Index')
                    ax.flatten()[idx].set_ylabel(r'Helical displacement $\AA$')
                else:
                    if p == 4 and (type(only_following_angle_indices) is not type(None)) and idx >= 2:
                        ax.flatten()[idx].set_xlabel(r'Angle $\degree$ Bins')
                        ax.flatten()[idx].set_ylabel(r'Counts')
                    else:
                        ax.flatten()[idx].set_xlabel('Time Series a.u.')
                        if p == 6:
                            ax.flatten()[idx].set_ylabel(r'Helical displacement $\AA$')
                        else:
                            ax.flatten()[idx].set_ylabel(r'Angular change $\degree$')

            fig.tight_layout()
            if p == 4 and (type(only_following_angle_indices) is not type(None)):
                fig.savefig(os.path.join("analysis_figures", "interpolations_"+f+"_selected.png"))
            else:
                fig.savefig(os.path.join("analysis_figures", "interpolations_"+f+".png"))

            plt.close()

        mpl.rcParams.update(mpl.rcParamsDefault) #https://stackoverflow.com/questions/26413185/how-to-recover-matplotlib-defaults-after-setting-stylesheet#:~:text=matplotlib%20as%20mpl-,mpl.rcParams.update(mpl.rcParamsDefault),-In%20ipython%2C%20things

    compare_data_stats()
    only_following_angle_indices = np.array([1,2,5,6,8,12]) - 1
    compare_data_stats(only_following_angle_indices=only_following_angle_indices)


    ###########################
    ##########TESTING##########
    ###########################

    #BELOW: code
    # python -m testSerT --pdb_file 3f48finaleqnbfix.pdb --trajectory_files force5TM18_water_nbfix_2.dcd --align_selection backbone --standard_file adk_fc.npy --atom_selection backbone --strategy none --batch_size 128 --scheduler reduce -gaccm 1 --train_mode analyze --num_epochs 15000 --lr 0.001 --which_model fc --truncate 512 --name adk_fc -ckpt adk_fc.ckpt --split_portion 100 --num_interps 102 --dssp_original_trajs adk_fc_test.dcd --dssp_recon_trajs adk_fc_recon.dcd --atom_pair_indices [0,1] [2,3]

    #BELOW: atom pairs from paper (Serotonin transporter–ibogaine complexes illuminate mechanisms of inhibition and transport)
    # DEPRECATED: FULL SERT: python -m testSerT --psf_file fornamd.psf --pdb_file fornamd.pdb --trajectory_files eq01.dcd --align_selection backbone --standard_file adk_fc.npy --atom_selection backbone --strategy none --batch_size 128 --scheduler reduce -gaccm 1 --train_mode analyze --num_epochs 15000 --lr 0.001 --which_model fc --truncate 512 --name adk_fc -ckpt adk_fc.ckpt --split_portion 100 --num_interps 102 --dssp_original_trajs adk_fc_test.dcd --dssp_recon_trajs adk_fc_recon.dcd --atom_pair_indices [654,706] [654,1851] [654,2918] [654,3671] [654,4148] [654,5011] [654,5523] [654,6475] [654,6556] [654,7686] [654,8012] --atom_angle_pair_indices [45,149] [169,277] [345,449] [721,773] [833,901] [1017,1101] [1137,1249] [1389,1517] [1553,1625] [1649,1765] [1845,1929] [2001,2101]
    # TM_SELECTION="(resid 85-111) or (resid 116-143) or (resid 160-186) or (resid 254-267) or (resid 282-299) or (resid 328-349) or (resid 358-386) or (resid 421-453) or (resid 462-480) or (resid 486-515) or (resid 535-556) or (resid 574-599)"
    # USE THIS: REDUCED SERT: python -m testSerT --pdb_file ref_SerT_reduced.pdb --trajectory_files eq01.dcd --align_selection "(resid 85-111) or (resid 116-143) or (resid 160-186) or (resid 254-267) or (resid 282-299) or (resid 328-349) or (resid 358-386) or (resid 421-453) or (resid 462-480) or (resid 486-515) or (resid 535-556) or (resid 574-599)" --atom_selection backbone --strategy none --batch_size 128 --scheduler reduce -gaccm 1 --train_mode analyze --num_epochs 15000 --lr 0.001 --which_model fc --truncate 512 --name adk_fc -ckpt adk_fc.ckpt --split_portion 100 --num_interps 102 --dssp_original_trajs adk_fc_test.dcd --dssp_recon_trajs adk_fc_recon.dcd --atom_pair_indices [149,169] [149,449] [149,721] [149,901] [149,1017] [149,1249] [149,1389] [149,1625] [149,1649] [149,1929] [149,2001] --atom_angle_pair_indices [45,149] [169,277] [345,449] [721,773] [833,901] [1017,1101] [1137,1249] [1389,1517] [1553,1625] [1649,1765] [1845,1929] [2001,2101]

    # EC: ag.select_atoms("resid 186 111 116 254 299 328 386 421 480 486 556 574 and name CA") 
    # WRONG: EC-pairs for full SerT: [654,706] [654,1851] [654,2918] [654,3671] [654,4148] [654,5011] [654,5523] [654,6475] [654,6556] [654,7686] [654,8012]
    # EC-pairs for reduced SerT: [449,149] [449,169] [449,721] [449,901] [449,1017] [449,1249] [449,1389] [449,1625] [449,1649] [449,1929] [449,2001]

    # IC: ag.select_atoms("resid 160 85 143 267 282 349 358 453 462 515 535 599  and name CA")
    # WRONG: IC-pairs for full SerT: [224,1141] [224,1438] [224,3137] [224,3385] [224,4445] [224,4586] [224,6016] [224,6178] [224,7001] [224,7314] [224,8410]
    # IC-pairs for reduced SerT: [345,45] [345,277] [345,773] [345,833] [345,1101] [345,1137] [345,1517] [345,1553] [345,1765] [345,1845] [345,2101]

    # EC&IC: "resid 115 254 299 574 282 462 535 599 and name CA"
    # EC&IC helical distances: [165,901] [721,901] [721,2001] [165,2001] [833,1553] [833,1845] [2101,1553] [2101,1845]
    # EC&IC vestibule: "resid 104 493 176 335 and name CA"
    # EC&IC vestibule distances: [121,1677] [409,1045]
    # angles: "resid 85 111 143 116  160 186 267 254 282 299 328 349 358 386 453 421 462 480 515 486 535 556 599 574 and name CA"
    # angles for reduced SerT: [45,149] [169,277] [345,449] [721,773] [833,901] [1017,1101] [1137,1249] [1389,1517] [1553,1625] [1649,1765] [1845,1929] [2001,2101]

