
# IGFOLD RELAX: https://github.com/Graylab/IgFold/blob/d3c0d10a69b34f2c6ccb7e550edad76500f6f0c5/igfold/refine/openmm_ref.py

import pdbfixer
import openmm
import os
from openmm.app.dcdreporter import DCDReporter
ENERGY = openmm.unit.kilocalories_per_mole
LENGTH = openmm.unit.angstroms
import ray
from openmm.app import *
from openmm import *
from openmm.unit import *

@ray.remote
def refine(pdb_file, stiffness=10., tolerance=2.39, use_gpu=False):
    tolerance = tolerance * ENERGY
    stiffness = stiffness * ENERGY / (LENGTH**2)

    #BELOW: https://github.com/openmm/pdbfixer/blob/db2886903fe835919695c465fd20a9ae3b2a03cd/pdbfixer/pdbfixer.py#L93:~:text=of%20PDBFixer%20object.-,%3E%3E%3E%20fixer%20%3D%20PDBFixer(pdbid%3D%271YRI%27),%3E%3E%3E%20fixer.replaceNonstandardResidues(),-%22%22%22
    pdbfixer.pdbfixer.substitutions.update(dict(HSE="HIS")) #HSE is not part of original subsitutions!
    fixer = pdbfixer.PDBFixer(pdb_file)
    fixer.findNonstandardResidues()
    fixer.replaceNonstandardResidues()
#     print(fixer.nonstandardResidues)

    fixer.findMissingResidues()
    fixer.findMissingAtoms()
    fixer.addMissingAtoms()

#     force_field = openmm.app.ForceField("amber14/protein.ff14SB.xml") charmm36.xml
    force_field = openmm.app.ForceField("charmm36.xml") 

    modeller = openmm.app.Modeller(fixer.topology, fixer.positions)
    modeller.addHydrogens(force_field)
    system = force_field.createSystem(modeller.topology, nonbondedMethod=PME,
                                        nonbondedCutoff=1*nanometer, constraints=HBonds)

    force = openmm.CustomExternalForce("0.5 * k * ((x-x0)^2 + (y-y0)^2 + (z-z0)^2)")
    force.addGlobalParameter("k", stiffness)
    for p in ["x0", "y0", "z0"]:
        force.addPerParticleParameter(p)
    for residue in modeller.topology.residues():
        for atom in residue.atoms():
            if atom.name in ["N", "CA", "C", "CB"]:
                force.addParticle(atom.index,
                                    modeller.positions[atom.index])
    system.addForce(force)

    integrator = openmm.LangevinIntegrator(0, 0.01, 1.0)
    platform = openmm.Platform.getPlatformByName("CUDA" if use_gpu else "CPU")

    simulation = openmm.app.Simulation(modeller.topology, system, integrator, platform)
    simulation.context.setPositions(modeller.positions)

#     simulation.reporters.append(DCDReporter('minimized.dcd', 100))
    simulation.minimizeEnergy(tolerance, maxIterations=10000)

    with open(os.path.splitext(pdb_file)[0] + "_fixed.pdb", "w") as f:
        openmm.app.PDBFile.writeFile(
            simulation.topology,
            simulation.context.getState(getPositions=True).getPositions(),
            f,
            keepIds=True,)

if __name__ == "__main__":
    refine("data/3f48finaleqnbfix_reduced.pdb", use_gpu=False)
    
    #IDPGAN: https://github.com/feiglab/idpgan/blob/main/notebooks/idpgan_experiments.ipynb (superposition of generated struct)
    # random_frame = np.random.choice(xyz_custom_gen.shape[0])
    # print("- Showing frame %s" % random_frame)
    # m_traj = mdtraj.Trajectory(
    #                 xyz_custom_gen[random_frame,:,:].reshape(1, -1, 3),
    #                 mdtraj.load(top_fp).topology)

    #IDPGAN: use below https://github.com/feiglab/idpgan/blob/main/idpgan/plot.py
    # abs_dmap_custom_gen = get_distance_matrix(abs_xyz_custom_gen)
    # abs_cmap_custom_gen = get_contact_map(abs_dmap_custom_gen)
    # abs_rg_custom_gen = compute_rg(abs_xyz_custom_gen)
    # abs_dh_custom_gen = torch_chain_dihedrals(torch.tensor(abs_xyz_custom_gen)).numpy()

# from openmm.app import *
# from openmm import *
# from openmm.unit import *
# from sys import stdout, exit, stderr

# psf = CharmmPsfFile('input.psf')
# pdb = PDBFile('input.pdb')
# params = CharmmParameterSet('charmm22.rtf', 'charmm22.prm')
# system = psf.createSystem(params, nonbondedMethod=NoCutoff,
#         nonbondedCutoff=1*nanometer, constraints=HBonds)
# integrator = LangevinMiddleIntegrator(300*kelvin, 1/picosecond, 0.004*picoseconds)
# simulation = Simulation(psf.topology, system, integrator)
# simulation.context.setPositions(pdb.positions)
# simulation.minimizeEnergy()
# # simulation.reporters.append(PDBReporter('output.pdb', 1000))
# simulation.reporters.append(StateDataReporter(stdout, 1000, step=True,
#         potentialEnergy=True, temperature=True))
# simulation.reporters.append(DCDReporter('output.dcd', 1000))

# simulation.step(10000)
