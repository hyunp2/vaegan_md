import torch
import torch.nn as nn
import einops
from einops.layers.torch import Rearrange
import itertools
import numpy as np
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
import os
import MDAnalysis as mda
import warnings
import gc
import inspect
import curtsies.fmtfuncs as fmt
from typing import *
import copy
import pytorch_lightning as pl
from MDAnalysis.analysis import align
from MDAnalysis.coordinates.memory import MemoryReader
from MDAnalysis.analysis.base import AnalysisFromFunction
import argparse
import dataloader as dl 
from loss_utils import *

warnings.simplefilter("ignore")

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
torch.set_printoptions(precision=4)

###########Neural ODE##############
from torchdiffeq import odeint_adjoint as odeint



class CNF(nn.Module):
    """Adapted from the NumPy implementation at:
    https://gist.github.com/rtqichen/91924063aa4cc95e7ef30b3a5491cc52
    """
    def __init__(self, in_out_dim, hidden_dim, width):
        super().__init__()
        self.in_out_dim = in_out_dim
        self.hidden_dim = hidden_dim
        self.width = width
        self.hyper_net = HyperNetwork(in_out_dim, hidden_dim, width)

    def forward(self, t, states):
        z = states[0]
        logp_z = states[1]

        batchsize = z.shape[0]

        with torch.set_grad_enabled(True):
            z.requires_grad_(True)

            W, B, U = self.hyper_net(t)

            Z = torch.unsqueeze(z, 0).repeat(self.width, 1, 1)

            h = torch.tanh(torch.matmul(Z, W) + B)
            dz_dt = torch.matmul(h, U).mean(0)

            dlogp_z_dt = -trace_df_dz(dz_dt, z).view(batchsize, 1)

        return (dz_dt, dlogp_z_dt)


def trace_df_dz(f, z):
    """Calculates the trace of the Jacobian df/dz.
    Stolen from: https://github.com/rtqichen/ffjord/blob/master/lib/layers/odefunc.py#L13
    """
    sum_diag = 0.
    for i in range(z.shape[1]):
        sum_diag += torch.autograd.grad(f[:, i].sum(), z, create_graph=True)[0].contiguous()[:, i].contiguous()

    return sum_diag.contiguous()


class HyperNetwork(nn.Module):
    """Hyper-network allowing f(z(t), t) to change with time.
    Adapted from the NumPy implementation at:
    https://gist.github.com/rtqichen/91924063aa4cc95e7ef30b3a5491cc52
    """
    def __init__(self, in_out_dim, hidden_dim, width):
        super().__init__()

        blocksize = width * in_out_dim

        self.fc1 = nn.Linear(1, hidden_dim)
        self.fc2 = nn.Linear(hidden_dim, hidden_dim)
        self.fc3 = nn.Linear(hidden_dim, 3 * blocksize + width)

        self.in_out_dim = in_out_dim
        self.hidden_dim = hidden_dim
        self.width = width
        self.blocksize = blocksize

    def forward(self, t):
        # predict params
        params = t.reshape(1, 1)
        params = torch.tanh(self.fc1(params))
        params = torch.tanh(self.fc2(params))
        params = self.fc3(params)

        # restructure
        params = params.reshape(-1)
        W = params[:self.blocksize].reshape(self.width, self.in_out_dim, 1)

        U = params[self.blocksize:2 * self.blocksize].reshape(self.width, 1, self.in_out_dim)

        G = params[2 * self.blocksize:3 * self.blocksize].reshape(self.width, 1, self.in_out_dim)
        U = U * torch.sigmoid(G)

        B = params[3 * self.blocksize:].reshape(self.width, 1, 1)
        return [W, B, U]


##################################
class Encoder(torch.nn.Module):
    def __init__(self, hidden_dims=[1000, 500, 100, 50, 4], **kwargs):
        super().__init__()
        self.hidden_dims = hidden_dims
        self.unrolled_dim = kwargs.get("unrolled_dim") #xyz coord dim of original protein trajectory

        linears = torch.nn.Sequential(*[ 
                                      torch.nn.Linear(self.unrolled_dim, self.hidden_dims[0]), torch.nn.SiLU(), 
                                      torch.nn.Linear(self.hidden_dims[0], self.hidden_dims[1]), torch.nn.SiLU(True),                 
                                      torch.nn.Linear(self.hidden_dims[1], self.hidden_dims[2]), torch.nn.SiLU(True),
                                      torch.nn.Linear(self.hidden_dims[2], self.hidden_dims[3]), torch.nn.SiLU(True),                
                                      torch.nn.Linear(self.hidden_dims[3], self.hidden_dims[4]),                     
                                    ]) #B,2
        self.add_module("linears_sequential", linears)

    def forward(self, inputs):
        sizes = inputs.size()
        x = inputs #BLC -> Cartesian coords...
        x = x.view(sizes[0], -1)
        x = self.linears_sequential(x)
        mu, logstd = torch.chunk(x, 2, dim=-1)
        z = self.reparameterize(mu, logstd)
        return z, mu, logstd

    def reparameterize(self, mu, logstd):
        shapes = mu.shape
        return mu + logstd.exp() * torch.distributions.Normal(0., 0.1).rsample((shapes)).to(mu)

class Decoder(torch.nn.Module):
    def __init__(self, hidden_dims=list(reversed([2, 50, 100, 500, 1000])), **kwargs):
        super().__init__()
        self.hidden_dims = hidden_dims
        self.unrolled_dim = kwargs.get("unrolled_dim") #xyz coord dim of original protein trajectory
#         self.rolled_dim = kwargs.get("rolled_dim", None) #xyz coord dim of original protein trajectory
#         self.reference = kwargs.get("reference", None) #PDB of reference
#         self.mha_dimension = kwargs.get("mha_dimension", None)
#         self.nheads = kwargs.get("nheads", None)
#         self.layers = kwargs.get("layers", None)

        linears = torch.nn.Sequential(*[ 
                                      torch.nn.Linear(self.hidden_dims[0], self.hidden_dims[1]), torch.nn.SiLU(True),                 
                                      torch.nn.Linear(self.hidden_dims[1], self.hidden_dims[2]), torch.nn.SiLU(True),
                                      torch.nn.Linear(self.hidden_dims[2], self.hidden_dims[3]), torch.nn.SiLU(True),                
                                      torch.nn.Linear(self.hidden_dims[3], self.hidden_dims[4]), torch.nn.SiLU(True),           
                                      torch.nn.Linear(self.hidden_dims[4], self.unrolled_dim)
                                    ]) #B,C,H,W
        self.add_module("linears_sequential", linears)
        self.unrolled_size = self.hidden_dims[1] #change this based on context!
        self.func = CNF(self.unrolled_size, 32, 32)
        self.neural_ode = odeint
        # self.act = torch.nn.Sigmoid()
        #self.mha_res = MultiheadAttention_Residual(rolled_dim=self.rolled_dim, mha_dimension=self.mha_dimension, nheads=self.nheads)
        #feedforward = torch.nn.Sequential(*[torch.nn.Linear(self.rolled_dim, self.rolled_dim), torch.nn.LeakyReLU(True), torch.nn.Linear(self.rolled_dim, self.rolled_dim)])
        #self.add_module("ff", feedforward)
        #self.pos_emb = torch.nn.Embedding(self.reference.size(1), 3) #reference is (1,L,3)

    def forward(self, inputs: "BD"):
        sizes = (1, self.unrolled_dim//3, 3) #1,L,3
        x = inputs #Latent dim
        x = self.linears_sequential[1](self.linears_sequential[0](x)) #output of first/second linear

#         x = 3*torch.tanh(x)
        # x_q = self.act(x_q)
        logp_diff_t1 = x.new_zeros(x.size(0), 1)
        x, logp_diff_t = self.neural_ode(self.func,                
                                    (x, logp_diff_t1),
                                    torch.linspace(1, 0, 2).to(x),
                                    #torch.tensor([10, 0]).type(torch.float32).to(device),
                                    atol=1e-5,
                                    rtol=1e-5,
                                    method='dopri5')
        x, logp_diff_t = x[-1], logp_diff_t[-1] #Last time for (B,dim)
        for i in range(2, len(self.linears_sequential)):
            x = self.linears_sequential[i](x)
        x_q = x
        # print(x.size())
        x_q = x.view(x.size(0), sizes[1], sizes[2]) #+ pos_emb #B,L,3 

        return (x_q, logp_diff_t) #, attns


class NeuralVAE(torch.nn.Module):
    #VAE github: https://github.com/AntixK/PyTorch-VAE/tree/master/models
    """Input and output are both (B,L,3) and flattend inside Encoder/Decoder!"""
    def __init__(self, args: argparse.ArgumentParser, **kwargs):
        super().__init__()
        self.args = args
        self.hidden_dims_enc = kwargs.get("hidden_dims_enc", None)
        self.hidden_dims_dec = kwargs.get("hidden_dims_dec", None)
        self.unrolled_dim = kwargs.get("unrolled_dim", None) #xyz coord dim of original protein trajectory
#         self.rolled_dim = kwargs.get("rolled_dim") #xyz coord dim of original protein trajectory
#         self.reference = kwargs.get("reference") #PDB of reference
#         self.mha_dimension = kwargs.get("mha_dimension", 1200)
#         self.nheads = kwargs.get("nheads", 6)
#         self.layers = kwargs.get("layers", 6)
        self.encoder = Encoder(hidden_dims=self.hidden_dims_enc, unrolled_dim=self.unrolled_dim)
#         self.decoder = Decoder(hidden_dims=self.hidden_dims_dec, reference=self.reference, rolled_dim=self.rolled_dim, unrolled_dim=self.unrolled_dim, mha_dimension=self.mha_dimension, nheads=self.nheads, layers=self.layers)
        self.decoder = Decoder(hidden_dims=self.hidden_dims_dec, unrolled_dim=self.unrolled_dim)
#         self.apply(self._init_weights)
        self.reset_all_weights()
    
    def forward(self, inputs: "Trajectory"):
        x = inputs #Normalized input
        z, mu, logstd = self.encoder(x)
        (x, logp) = self.decoder(z) #BL3, Dict: BHLL x Layers
        return z, mu, logstd, (x, logp)
    
    @staticmethod
    def losses(inputs, z, mu, logstd, x_logp: "(recon, logp)", beta, mean, std ):
        recon, logp = x_logp
        mse = torch.nn.MSELoss(reduction="none")(recon, inputs).mean(dim=(1,2)) # -> (B,)
        kl = torch.sum(-0.5 * beta * (1 + logstd - mu ** 2 - logstd.exp()), dim = 1)  #kl-div (NOT a LOSS yet!); -> (B,)
        logp = logp

        if 0:
            unnormalize = dl.ProteinDataset.unnormalize #static method
            original_unscaled = unnormalize(inputs, mean=mean, std=std) #BL3
            recon_unscaled = unnormalize(recon, mean=mean, std=std)
            X = original_unscaled.permute(0,2,1) #B3L
            Y = recon_unscaled.permute(0,2,1) #B3L

            X = original_unscaled #->B3L
            Y = recon_unscaled #->B3L
            X, Y = kabsch_torch(X, Y) #-> (B,3,L) alignment: translated and rotated to fit!

            rmsd = rmsd_torch(X, Y) # -> (B,)
            gdt = gdt_torch(X, Y, torch.arange(0.5, 10.5, 0.5).to(X)) # -> (B,)
            tm = tmscore_torch(X, Y) # -> (B,)

        rmsd = kl.new_zeros(kl.size())
        gdt = kl.new_zeros(kl.size())
        tm = kl.new_zeros(kl.size())

        assert mse.size(0) == kl.size(0) and mse.ndim == kl.ndim and mse.ndim == 1, "all criteria for shape must match"
        assert mse.size(0) == rmsd.size(0) and mse.size(0) == gdt.size(0) and mse.size(0) == tm.size(0), "all criteria for shape must match"
        return mse, kl, rmsd, gdt, (tm, logp)

    def reset_all_weights(self, ) -> None:
        """
        refs:
        - https://discuss.pytorch.org/t/how-to-re-set-alll-parameters-in-a-network/20819/6
        - https://stackoverflow.com/questions/63627997/reset-parameters-of-a-neural-network-in-pytorch
        - https://pytorch.org/docs/stable/generated/torch.nn.Module.html
        """

        @torch.no_grad()
        def weight_reset(m: torch.nn.Module):
             # - check if the current module has reset_parameters & if it's callabed called it on m
            reset_parameters = getattr(m, "reset_parameters", None)
            if callable(reset_parameters):
                m.reset_parameters()

        # Applies fn recursively to every submodule see: https://pytorch.org/docs/stable/generated/torch.nn.Module.html
        self.apply(fn=weight_reset)


