#!/usr/bin/env python
import os
import argparse
import subprocess
from math import floor
from subprocess import run, Popen, PIPE

parser = argparse.ArgumentParser()
parser.add_argument('namd_conf',type=str,help="NAMD Configuration File")
parser.add_argument('--nodes',type=int,default=1,help="Number of Nodes Requested")
parser.add_argument('gpus_per_node',type=int,default=4,help="GPUs per Node Requested")
parser.add_argument('-n','--name',type=str,default='namd2',help="Job Name")
parser.add_argument('-l','--logfile',type=str,help="NAMD Output Log File")
parser.add_argument('-w','--walltime',type=float,default=48, help="Job Running Time (Hours)")
parser.add_argument('-CpG',type=int,default=1, help="CPUs per GPU")
parser.add_argument('-pmepes',type=int,default=1, help="PEs per GPU")
parser.add_argument('-q','--queue',default='gpuA100x4',type=str,help="Slurm Partitions, Choose From gpuA100x4, gpuA100x4-interactive, gpuA100x8, gpuA100x8-interactive, gpuA40x4, gpuA40x4-interactive, gpuMI100x8, gpuMI100x8-interactive")
#parser.add_argument('-r', '--replicas',type=int,help='Number of replicas')
parser.add_argument('--namdopts',type=str,default='',help='Additional options for NAMD (e.g. "+stdout %j.log"')
parser.add_argument('-d','--depend',type=str,help="Hold Job for Running")

args = parser.parse_args()

if not args.nodes:
    args.nodes = 1
if args.nodes > 1:
    print("WARNING: NAMD3 only supports single node currently! Changing to 1 node...")
    args.nodes = 1

if args.walltime > 48:
    print("WARNING: Maximum walltime for non-interactive queue is 48 hours! Changing to 48 hours...")
    args.walltime = 48

keyword = 'interactive'
if keyword in args.queue:
    if args.walltime > 1:
        print("WARNING: Maximum walltime for interactive queue is 1 hour! Changing to 1 hour...")
        args.walltime = 1

for filename in [args.namd_conf]:
    if not os.path.isfile(filename):
        raise Exception("NAMD Configuration File {} not Found!".format(filename))

def format_walltime(total):
    t = total
    h = floor(t)
    m = floor((t - h) * 60)
    s = floor(((t - h) * 60 - m) * 60)
    return ":".join( ["%02d" % i for i in (h,m,s)] )

data = vars(args)

data["walltime"] = format_walltime( args.walltime )
data["cpus_per_task"] = (data["gpus_per_node"]-1) * data["CpG"] + data["pmepes"]

data["device_seq"] = range(0,data["gpus_per_node"])
data["device"] = ",".join(list(map(str,data["device_seq"])))

if not args.depend:
    data["depend"] = ""
else:
    data["depend"] = "#SBATCH --dependency=afterany:" + args.depend

data["fname"] = args.namd_conf[:-5]
if not args.logfile:
    data['logfile'] = '{}.log'.format(args.namd_conf[:-5])

if os.path.isfile(data['logfile']):
    raise Exception("Log file {} already exists!".format(data['logfile']))

run_script = """#! /bin/bash
#SBATCH -J {name}
#SBATCH --output="{fname}.%j.out"

#SBATCH --account=kkf-delta-gpu
#SBATCH --nodes={nodes}      # Total number of nodes
#SBATCH --mem=240g
#SBATCH -p {queue}
#SBATCH -t {walltime}
#SBATCH --gpus-per-node={gpus_per_node}
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task={cpus_per_task}
#SBATCH --gpu-bind=none
{depend}

module load gcc/11.2.0
module load openmpi/4.1.2
module load fftw/3.3.10

srun /sw/namd/NAMD_3.0b2_Linux-x86_64-multicore-CUDA/namd3 +setcpuaffinity +p{cpus_per_task} +pmepes {pmepes} +devices {device} {namdopts} "{namd_conf}" >& "{logfile}"
""".format(**data)

##Use subprocess.Popen 
#p = Popen('sbatch', stdin=PIPE, stdout=PIPE, stderr=PIPE)
#out,err = p.communicate(run_script.encode("utf-8"))
#print(out.decode("utf-8"))
#print(err.decode("utf-8"))

#Or use subprocess.run
p = run(['sbatch'], input=run_script.encode("utf-8"))
out, err = p.stdout, p.stderr
print(out)
print(err)

if p.returncode != 0:
    raise Exception("Problem submitting script with sbatch")




