import torch
import einops
from einops.layers.torch import Rearrange
import itertools
import numpy as np
import matplotlib.pyplot as plt
from curtsies import fmtfuncs as cf
from sklearn.manifold import TSNE
import os
import MDAnalysis as mda
import warnings
import gc
import inspect
import curtsies.fmtfuncs as fmt
from typing import *
import copy
import pytorch_lightning as pl
from MDAnalysis.analysis import align
from MDAnalysis.coordinates.memory import MemoryReader
from MDAnalysis.analysis.base import AnalysisFromFunction
from MDAnalysis import transformations

# import main as Main
import pdb as PDB
from MDAnalysis.analysis.align import AlignTraj
import ray
import argparse

__all__ = ["DataModule", "alignTrajectory"]

# @ray.remote
def alignTrajectory(args):
    atom_selection = args.atom_selection
    align_selection = args.align_selection
    assert args.pdb_file != None or args.psf_file != None, "either PDB and PSF must be provides..."
    #PDB.set_trace()
    # print(args.load_data_directory, args.pdb_file, os.path.join(args.load_data_directory, args.pdb_file))
    pdb = os.path.join(args.load_data_directory, args.pdb_file) #string
    # print(type(args.psf_file))
    psf = os.path.join(args.load_data_directory, args.psf_file) if args.psf_file != None else None #string
    # print(psf)
    print(cf.red(f"Atoms to extract and atoms to align are the same: {len(mda.Universe(pdb).select_atoms(atom_selection)) == len(mda.Universe(pdb).select_atoms(align_selection)) }"))
    traj = list(map(lambda inp: os.path.join(args.load_data_directory, inp), args.trajectory_files)) #string list
    prot_ref = mda.Universe(psf, pdb) if psf != None else mda.Universe(pdb) #must not call PSF
    if psf != None and pdb != None:
        prot_traj = mda.Universe(psf, *traj)
    elif psf == None and pdb != None:
        prot_traj = mda.Universe(pdb, *traj)
 
    print(f"Before alignment unweighted COM: {prot_traj.select_atoms(atom_selection).positions.mean(axis=1)}")

    #Below for cases where dimensions are not set properly
    #1. MDA transformation is on-the-fly transformation
    #2. MDA to memory is permanent change (which may have to come after transformation)
    #3. MDA analysis is analysis (which may have to come after transformation and/or memory)
    
    if (prot_traj.trajectory.ts.dimensions is not None): 
        box_dim = prot_traj.trajectory.ts.dimensions
    else:
        # box_dim = np.array([1,1,1,90,90,90])
        box_dim = np.array([ 97.38272,  97.38272, 108.14863,  90.     ,  90.     ,  90.     ]) #Average heuristics of SerT

#         print(box_dim, prot_traj.atoms.positions, prot_ref.atoms.positions, align_selection)
    transform = transformations.boxdimensions.set_dimensions(box_dim)
    prot_traj.trajectory.add_transformations(transform)

    AlignTraj(prot_traj, prot_ref, select=align_selection, in_memory=True).run()
    print(f"After alignment unweighted COM: {prot_traj.select_atoms(atom_selection).positions.mean(axis=1)}")
    return prot_traj, prot_ref, atom_selection #in-memory modifed!
    
def extract_trajectory(args):
    prot_traj, prot_ref, atom_selection = alignTrajectory(args) #Aligned prot_traj in-memory
    prot_ref_ag = prot_ref.atoms.select_atoms(atom_selection) 
    print("Returning RMSD aligned universe instances...")
    
    reduced_pdb_file = os.path.join(args.load_data_directory, os.path.splitext(args.pdb_file)[0] + "_reduced.pdb")
#     if not os.path.exists(reduced_pdb_file):
    prot_ref_ag.write(reduced_pdb_file) #write a reduced file; based on atom selection!
#     else:
#         pass
    reference = torch.from_numpy(prot_ref_ag.positions)[None,:]
    coords = AnalysisFromFunction(lambda ag: ag.positions.copy(),
                                   prot_traj.atoms.select_atoms(f"{atom_selection}")).run().results['timeseries']
    trajectory = torch.from_numpy(coords) #Train
    assert reference.ndim == trajectory.ndim, "Must have 3 dimensions for both REF and TRAJ..."
    assert isinstance(reference, torch.Tensor) and isinstance(trajectory, torch.Tensor), "Both reference and trajectory should be torch tensors!"
    print(cf.on_yellow(f"Protesin has {trajectory.size(1)} atoms selected!"))
    return reference, trajectory #Both are atom_selection (i.e. potentially reduced!)

class ProteinDataset(torch.utils.data.Dataset):
    """Normalized dataset and reverse-normalization happens here..."""
    def __init__(self, args: argparse.ArgumentParser, dataset: List[Union[torch.Tensor, np.array]]):
        super().__init__()
        self.reference = dataset[0]
        self.trajectory = dataset[1]
        if os.path.exists(args.standard_file):
            print(cf.on_yellow(f"Standard dataset {args.standard_file} exists..."))
            mean_and_std_of_trained = torch.from_numpy(np.load(args.standard_file))
            mean, std = mean_and_std_of_trained[0], mean_and_std_of_trained[1]
            self.trajectory, self.mean, self.std = self.normalize(self.trajectory, mean, std) #Raw (x,y,z) to Normalized (x,y,z)
            # self.min, self.max = self.trajectory.min(), self.trajectory.max()
            # self.trajectory = (self.trajectory - self.min) / (self.max - self.min) #2nd normalization; range [0-1]
        else:
            print(cf.red(f"Standard dataset {args.standard_file} DOES NOT exists..."))
            self.trajectory, self.mean, self.std = self.normalize(self.trajectory) #Raw (x,y,z) to Normalized (x,y,z)
            np.save(args.standard_file, torch.stack([self.mean, self.std], dim=0).detach().cpu().numpy() )
            # self.min, self.max = self.trajectory.min(), self.trajectory.max()
            # self.trajectory = (self.trajectory - self.min) / (self.max - self.min) #2nd normalization; range [0-1]
        assert self.reference.ndim == 3 and self.trajectory.ndim == 3, "dimensions are incorrect..."
        self.min = None
        self.max = None
        
    def __len__(self):
        return len(self.trajectory) #train length...
    
    def __getitem__(self, idx):
        coords = self.trajectory[idx] #B, L, 3
        return coords
    
    def normalize(self, coords, mean=None, std=None):
        if mean == None or std == None:
            coords = coords.view(coords.size(0), -1)
            mean = coords.mean(dim=0) #(B,C)
            std = coords.std(dim=0) #(B,C)
            coords_ = (coords - mean) / std
            coords_ = coords_.view(coords.size(0), coords.size(1)//3 ,3) #Back to original shape (B,L,3)
            return coords_, mean, std #coords_ is SCALED BL3 shape dataset!
        else:
            coords = coords.view(coords.size(0), -1)
            coords_ = (coords - mean) / std
            coords_ = coords_.view(coords.size(0), coords.size(1)//3 ,3) #Back to original shape (B,L,3)
            return coords_, mean, std #coords_ is SCALED BL3 shape dataset!

    @staticmethod
    def unnormalize(coords, mean=None, std=None, min=None, max=None, index: torch.LongTensor=None):
        #min max index are placeholders, doing nothing...
        assert mean != None and std != None, "Wrong arguments..."
        # coords = coords * (max - min) + min
        coords = coords.view(coords.size(0), -1)
        # print(coords.device)
        # print(mean.device)
        coords_ = (coords * std) + mean
        coords_ = coords_.view(coords.size(0), coords.size(1)//3 ,3) #Back to original shape (B,L,3)
        return coords_ #Reconstructed unscaled (i.e. raw) dataset (BL3)

class ProteinDatasetDistogram(torch.utils.data.Dataset):
    """Normalized dataset and reverse-normalization happens here..."""
    def __init__(self, args: argparse.ArgumentParser, dataset: List[Union[torch.Tensor, np.array]]):
        super().__init__()
        self.args = args
        self.reference = dataset[0]
        self.trajectory = dataset[1]
        if os.path.exists(args.standard_file_distogram):
            print(cf.on_yellow(f"Standard dataset {args.standard_file_distogram} exists..."))
            mean_and_std_of_trained = torch.from_numpy(np.load(args.standard_file_distogram))
            mean, std = mean_and_std_of_trained[0], mean_and_std_of_trained[1]
            self.trajectory, self.mean, self.std = self.normalize(self.trajectory, mean, std) #Raw (x,y,z) to Normalized (x,y,z)
            self.min, self.max = self.trajectory.min(), self.trajectory.max()
            self.trajectory = (self.trajectory - self.min) / (self.max - self.min) #2nd normalization; range [0-1]
        else:
            print(cf.red(f"Standard dataset {args.standard_file_distogram} DOES NOT exists..."))
            self.trajectory, self.mean, self.std = self.normalize(self.trajectory) #Raw (x,y,z) to Normalized (x,y,z)
            np.save(args.standard_file_distogram, torch.stack([self.mean, self.std], dim=0).detach().cpu().numpy() )
            self.min, self.max = self.trajectory.min(), self.trajectory.max()
            self.trajectory = (self.trajectory - self.min) / (self.max - self.min) #2nd normalization; range [0-1]
        assert self.reference.ndim == 3 and self.trajectory.ndim == 4, "dimensions are incorrect..."
        # self.min = None
        # self.max = None
        
    def __len__(self):
        return len(self.trajectory) #train length...
    
    def __getitem__(self, idx):
        assert isinstance(self.args.truncate, int), "truncate argument for window selection for distogram must be an integer..."
        b, c, d0, d1 = self.trajectory.size()
        
        if self.args.truncate > 0:
            start = np.random.randint(d0 - self.args.truncate) #choose an integer btw [0, d-truncate]
            end = start + self.args.truncate
            dists = self.trajectory[idx, :, start:end, start:end] #B,1,trunc,trunc
            # print(start, end, dists.shape)
            dists = dists.contiguous()
            return dists #, torch.LongTensor([start, end])
        else:
            dists = self.trajectory[idx]
            dists = dists.contiguous()
            return dists #, None

    def normalize(self, coords, mean=None, std=None):
        batch_dists = torch.cdist(coords, coords) #BLL
        if mean == None or std == None:
            mean = batch_dists.mean(dim=0) #(LL)
            std = batch_dists.std(dim=0) #(LL)
            dists_ = (batch_dists - mean) / std #BLL
            dists_ = dists_[:,None,...] #->B1LL
            return dists_, mean, std #B1LL, LL, LL
        else:
            dists_ = (batch_dists - mean) / std
            dists_ = dists_[:,None,...] #->B1LL
            return dists_, mean, std  #B1LL, LL, LL

    @staticmethod
    def unnormalize(dists, mean=None, std=None, min=None, max=None, index: torch.LongTensor=None):
        # print(dists.shape, mean.shape, std.shape)
        b, _, l0, l1 = dists.size() #B1LL
        dists = dists.view(b, l0, l1) #-> BLL
        dists = dists * (max - min) + min
        assert mean != None and std != None and min != None and max != None, "Wrong arguments..."
        dists_ = (dists * std) + mean #BLL
        # dists_ = dists_[:,index[:,0]:index]
        #WIP for indexing of distance!
        # if index[0] is not None:
        #     dists_ = torch.stack([dists_[i, s:e, s:e] for i, (s,e) in enumerate(index.unbind(dim=0))], dim=0) #->BLL
        # else:
        #     pass
        return dists_ #Reconstructed unscaled BLL

class DataModule(pl.LightningDataModule):
    def __init__(self, args=None, **kwargs):
        super(DataModule, self).__init__()
        datasets = extract_trajectory(args) #tuple of reference and traj
        self.dataset = ProteinDataset(args, datasets) if not args.distogram else ProteinDatasetDistogram(args, datasets)
        # print(self.dataset[0].shape)
        self.reference = self.dataset.reference #Reference data of (1,L,3); This is Unscaled (real XYZ)
        # self.trajectory = self.dataset.trajectory #Trajectory (B,L,3)
        self.mean = self.dataset.mean
        self.std = self.dataset.std
        self.min = self.dataset.min
        self.max = self.dataset.max
     
        self.batch_size = args.batch_size
        split_portion   = args.split_portion
        split_method    = args.split_method
        self.seed = kwargs.get("seed", 42)          
        if split_method == 'end': 
            split_portion = split_portion[0]
            assert split_portion <= 100 and split_portion > 0, "this parameter must be a positive number equal or less than 100..."
            self.split_portion = (split_portion / 100) if split_portion > 1 else split_portion
            self.train_data_length = int(len(self.dataset) * self.split_portion)
            self.valid_data_length = int(len(self.dataset) * (1 - self.split_portion)/2 )
            
            self.train_val_dataset = self.dataset[ : (self.train_data_length+self.valid_data_length) ]
            self.test_dataset      = self.dataset[(self.train_data_length+self.valid_data_length) : ]
            self.split_portion = [self.split_portion]
            
        if split_method == 'middle': 
            print(len(split_portion))
            assert len(split_portion) == 2, "Need two numbers to indicate the testing data in the middle"
            assert split_portion[1] - split_portion[0] > 0, "second number need to be bigger than first number"
            assert max(split_portion) <= 100 and all(split_portion) > 0, "each parameter must be a positive number and the sum should equal or less than 100..."          
            self.split_portion = split_portion
            self.split_portion_begin = (split_portion[0] / 100) if split_portion[0] > 1 else split_portion[0]
            self.split_portion_end   = (split_portion[1] / 100) if split_portion[1] > 1 else split_portion[1]
            self.train_data_length_begin = int(len(self.dataset) * self.split_portion_begin)
            self.train_data_length_end   = int(len(self.dataset) * self.split_portion_end)
            self.train_data_length       = int(len(self.dataset) * self.split_portion_begin + len(self.dataset)*(1 - self.split_portion_end))
            self.valid_data_length       = int(len(self.dataset) * (self.split_portion_end - self.split_portion_begin)/2 )

            self.train_val_dataset  = self.dataset[np.r_[: self.train_data_length_begin + self.valid_data_length, self.train_data_length_end: len(self.dataset)]]
            self.test_dataset       = self.dataset[self.train_data_length_begin + self.valid_data_length : self.train_data_length_end]
        
        self.num_workers = args.num_workers

    #@pl.utilities.distributed.rank_zero_only
    def setup(self, stage=None):
        self.trainset, self.validset= torch.utils.data.random_split(self.train_val_dataset, [self.train_data_length, self.valid_data_length], generator=torch.Generator().manual_seed(self.seed)) 
        self.testset = self.test_dataset #Pristine Last frames in correct times...
        
    def train_dataloader(self):
        return torch.utils.data.DataLoader(self.trainset, shuffle=True, num_workers=self.num_workers, batch_size=self.batch_size, drop_last=False, pin_memory=True)

    def val_dataloader(self):
        return torch.utils.data.DataLoader(self.validset, shuffle=False, num_workers=self.num_workers, batch_size=self.batch_size, drop_last=False, pin_memory=True)

    def test_dataloader(self):
        if not self.split_portion[0] in [100., 1.]:
            return torch.utils.data.DataLoader(self.testset, shuffle=False, num_workers=self.num_workers, batch_size=self.batch_size, drop_last=False, pin_memory=True)
        else:
            return torch.utils.data.DataLoader(self.dataset, shuffle=False, num_workers=self.num_workers, batch_size=self.batch_size, drop_last=False, pin_memory=True)

if __name__ == "__main__":
    from main import get_args
    args = get_args()

#     params = list(zip(itertools.repeat(lipid_analysis),
#                     itertools.repeat(n_workers),
#                     range(n_workers)))
#     ray.init()
    
#     futures = [parallelize_run.remote(*par) for par in params]
#     analyses = ray.get(futures)
    alignTrajectory(args)
    
    
#     args = Main.get_args()
#     args.batch_size = 50
#     reference, trajectory = extract_trajectory(args)
#     dataset = ProteinDataset([reference, trajectory])
#     dmo = DataModule(dataset, args, seed=42)
#     dmo.prepare_data()
#     dmo.setup()
#     print(next(iter(dmo.train_dataloader())))
#     print((ProteinDataset.unnormalize(next(iter(dmo.train_dataloader())), dataset.mean, dataset.std)))
#     #python dataloader.py -psf reference_autopsf.psf -pdb reference_autopsf.pdb -traj adk.dcd

