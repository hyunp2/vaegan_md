import glob
import captum
import deprecation
import attrs
import ase
from ase.io import Trajectory, read
from curtsies import fmtfuncs as cf
from typing import *
import networkx as nx
#import community as community_louvain
import numpy as np
import pandas as pd
import os, sys, gc, glob, time
from collections import OrderedDict
import collections
import warnings
import torch
import copy
import matplotlib.pyplot as plt
from mpl_toolkits import mplot3d
import deeptime
import pyemma
import tqdm
import curtsies.fmtfuncs as curt_fmt
import mdtraj
import torch
import pytorch_lightning as pl
import argparse
import logging
from typing import *
import tqdm
from curtsies import fmtfuncs as cf
import ase

# https://github.com/hyunp2/EBM/tree/main/Legacy

warnings.simplefilter('ignore')
np.set_printoptions(precision=10)
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
#np.random.seed(42)
torch.manual_seed(42)
torch.set_printoptions(precision=10, threshold=None, edgeitems=None, linewidth=None, profile=None, sci_mode=None)
pl.seed_everything(42)

logging.basicConfig()
logger = logging.getLogger("MSM Logger")
logger.setLevel(logging.DEBUG)

def get_highlights(self, inputs=None, attribute="saliency", normed=False):
    self.model.ignore_pair = True 
    args = self.args
    dmo = self.dmo
    test_data = next(iter(dmo.test_dataloader())).requires_grad_(True) if inputs == None else inputs.requires_grad_(True)

    if attribute == "saliency":
        from captum.attr import Saliency
        highlight = Saliency(self.model)
    if attribute == "integrated_gradients":
        from captum.attr import IntegratedGradients
        highlight = IntegratedGradients(self.model)      

    results = highlight.attribute(test_data, target=None)
    attr = results[0] if len(results) == 2 else results
    if normed:
        return attr.norm(p=2, dim=-1).detach().cpu().numpy()  #B,L
    else:
        return attr.detach().cpu().numpy()

#Rama
def plot_dihedral_ADP(args: argparse.ArgumentParser, model: pl.LightningModule, dmo: pl.LightningDataModule, generated_dcd: "Filename of DCD file by Langevin sampling"):
    """Alanine dipeptide backbone phi and psi"""
    psf = os.path.join(args.load_data_directory, args.psf_file)
    pdb = os.path.join(args.load_data_directory, args.pdb_file)
    #psf = args.psf_file
    #pdb = args.pdb_file
    atom_selection = args.atom_selection
    traj = list(map(lambda inp: os.path.join(args.load_data_directory, inp), args.trajectory_files)) #string list
    #traj = args.trajectory_files

    #print(psf, pdb, traj)
    feat = pyemma.coordinates.featurizer(pdb) 
    feat.add_backbone_torsions(periodic=True)
    #print(feat)
    data = pyemma.coordinates.load(traj, features=feat)
    data_concatenated = np.concatenate(data) #(B, 2) ... This is for FES surface

    T = 2 #different starting points
    fig, ax = plt.subplots(T, 1, figsize=(10,10))
    for i, m in enumerate([model]):
        for t in range(T):
            try:
                reduced_data = pyemma.coordinates.load(os.path.join(args.load_model_directory, generated_dcd), features=feat) #Langevin
                pyemma.plots.plot_free_energy(*data_concatenated.T, ax=ax.flatten()[i*2 + t], cmap=plt.cm.get_cmap("jet")); #FES of original coordnates
                ax.flatten()[i*2 + t].scatter(*reduced_data.T); #Scatter of Langevin traj's dihedral
                ax.flatten()[i*2 + t].set_title(f"{type(m.energy_block).__name__.lower()}-{t}") #physnet is the name?
                ax.flatten()[i*2 + t].scatter(reduced_data[0,0], reduced_data[0,1], marker="x", c="r", s=100)     
            except Exception as e:
                logger.warn(e)
    plt.savefig(os.path.join(args.load_model_directory, f"ADP-FES.png"), format="png")
    plt.close()
    
    return data_concatenated

    """
    #selection criteria to visualize three metastable states...
    ind=np.where((data_concatenated[:,0]>0) & (data_concatenated[:,0]<2) & (data_concatenated[:,1]>-1) & (data_concatenated[:,1]<1))[0][::100] #small energy well indices
    ind2=np.where((data_concatenated[:,0]>-3) & (data_concatenated[:,0]<0) & (data_concatenated[:,1]>1) & (data_concatenated[:,1]<3))[0][::1000] #top energy well indices
    ind3=np.where((data_concatenated[:,0]>-3) & (data_concatenated[:,0]<0) & (data_concatenated[:,1]>-1) & (data_concatenated[:,1]<1))[0][::1000] #mid-left energy well indices
    feat_ = pyemma.coordinates.featurizer(pdb) 
    #ind = feat_.select_Heavy()
    data_ = pyemma.coordinates.load(files, features=feat_)
    dat = np.concatenate(data_)
    dat = torch.from_numpy(dat.reshape(-1,22,3)[:,feat_.select_Heavy(),:])
    spec = fixed_valid_s[0:1].expand(dat.size(0), -1)
    """

def get_one_vampnet(**kwargs):
    gpu = kwargs.get("gpu")
    data = kwargs.get("data")
    meta_n = kwargs.get("meta_n")

    print(cf.on_yellow(f"GPU usage: {gpu}"))
    device = torch.device("cuda") if gpu else torch.device("cpu")

    lobe = torch.nn.Sequential(
        torch.nn.BatchNorm1d(data.shape[1]),
        torch.nn.Linear(data.shape[1], 20), torch.nn.ELU(),
        torch.nn.Linear(20, 20), torch.nn.ELU(),
        torch.nn.Linear(20, 20), torch.nn.ELU(),
        torch.nn.Linear(20, 20), torch.nn.ELU(),
        torch.nn.Linear(20, 20), torch.nn.ELU(),
        torch.nn.Linear(20, meta_n),
        torch.nn.Softmax(dim=-1)  # obtain fuzzy probability distribution over output states 
    )
    lobe = lobe.to(device)
    lobe_timelagged = copy.deepcopy(lobe)

    vampnet = deeptime.decomposition.deep.VAMPNet(lobe=lobe, lobe_timelagged=lobe_timelagged, learning_rate=5e-4, device=device)
    return vampnet

def apply_vamp_clustering(data: "LIST of B by 3L NUMPY", filename: str, lagtime: int, meta_n: int, batch_size: int, seed=42, gpu=True):
    """lagtime for trajectory split
    meta_n for number of macro clusters
    One-time calculation with a specific lagtime..."""
    dataset = deeptime.util.data.TrajectoryDataset.from_trajectories(lagtime=lagtime, data=[data]) #data is numpy
    n_val = int(len(dataset)*.3) #30% as a val set
    train_data, val_data = torch.utils.data.random_split(dataset, [len(dataset) - n_val, n_val], generator=torch.Generator().manual_seed(seed))
    
    vampnet = get_one_vampnet(gpu=True, data=data, meta_n=meta_n)

    loader_train = torch.utils.data.DataLoader(train_data, batch_size=batch_size, shuffle=True, generator=torch.Generator().manual_seed(seed))
    loader_val = torch.utils.data.DataLoader(val_data, batch_size=len(val_data), shuffle=True, generator=torch.Generator().manual_seed(seed))
    vampnet.fit(loader_train, n_epochs=160,
                    validation_loader=loader_val, progress=tqdm.tqdm) #"Vampnet estimator" fitting 
    filename = filename.split(".")[0] + ".pt" #To ensure .pt is parsed...
    save_trained_vampnet(vampnet, f"{filename}") #See below!

    model = vampnet.fetch_model() # fetching "fitted model" 
    nets = collections.namedtuple("nets", ["vampnet", "vampnet_model"])
    
    return nets(vampnet, model) #named tuple
 
def vampnet_analysis(data: np.ndarray, dihedrals: np.ndarray, nets: collections.namedtuple, lagtimes: List[int], title: str, root: str) -> plt.subplots:
    """Multiple evaluations at different lagtimes (cheap!!) given an expensive transformation above (vampnet)
    vampnet transform --> Covariance --> Koopman"""
    vampnet = nets.vampnet
    model = nets.vampnet_model

    #fig, ax = plt.subplots(1,3)
    fig = plt.figure()
    ax = fig.subplots(1,3)
    axes = ax.flatten()

    axes[0].loglog(*vampnet.train_scores_custom.T, label='training')
    axes[0].loglog(*vampnet.validation_scores_custom.T, label='validation')
    axes[0].set_xlabel('step')
    axes[0].set_ylabel('score')
    axes[0].set_title('VAMP2')
    axes[0].legend();

    state_probabilities = model.transform(data)
    assignments = state_probabilities.argmax(1)
    axes[1].scatter(*dihedrals.T, c=assignments, s=5, alpha=.1)
    axes[1].set_title('Transformed state assignments');

    vamp_models = [deeptime.decomposition.VAMP(lagtime=lag, observable_transform=model).fit_fetch(data) for lag in tqdm.tqdm(lagtimes)] #Get CovarianceModel for a given lagtime then RETURN CovarianceKoopmanModel!

    ax_ = deeptime.plots.plot_implied_timescales(deeptime.util.validation.implied_timescales(vamp_models))
    #ax_ = deeptime.util.validation.implied_timescales(vamp_models).plot() #SAME
    ax_.set_yscale('log')
    ax_.set_xlabel('lagtime')
    ax_.set_ylabel('timescale')
    axes[2] = ax_

    fig.savefig(os.path.join(root, f"{title}_vampnet_analysis.png"))
    plt.close()

    grid = deeptime.plots.plot_ck_test(vamp_models[0].ck_test(vamp_models)) 
    #grid = vamp_models[0].ck_test(vamp_models).plot() #SAME; returns fig instance!
    #https://github.com/deeptime-ml/deeptime/blob/7617f57e341e62c79d5ba3e7ed3be5953663066a/deeptime/util/validation.py#:~:text=def%20ck_test(models%2C%20observable%3A%20Observable%2C%20test_model%3DNone%2C%20include_lag0%3DTrue%2C%20err_est%3DFalse%2C%20progress%3DNone)%3A
    #observable means expectation (aka, class KoopmanObservable(Observable))
    grid.figure.savefig(os.path.join(root, f"{title}_vampnet_analysis_ck.png"))
    plt.close()    

def save_trained_vampnet(vampnet: deeptime.decomposition.deep.VAMPNet, filename: str):
    try:
        params = vampnet.get_params()
        params.pop("optimizer") #remove optimizer only...
        filename = filename.split(".")[0] + ".pt" #To ensure .pt is parsed...
        params.update({"train_scores_custom": np.array(vampnet._train_scores), "validation_scores_custom": np.array(vampnet._validation_scores)})
        torch.save(params, f"{filename}")
        print(cf.on_yellow("Saved as a Torch model..."))
    except Exception as e:
        print(e)
    finally:
        print("Done!")

def load_trained_vampnet(filename: str, gpu = True, data = None, meta_n: int = None):
    try:
        vampnet = get_one_vampnet(gpu=True, data=data, meta_n=meta_n)
        filename = filename.split(".")[0] + ".pt" #To ensure .pt is parsed...
        ckpt = torch.load(f"{filename}")
        [setattr(vampnet, key, value) for key, value in ckpt.items()]
        nets = collections.namedtuple("nets", ["vampnet", "vampnet_model"])
        Nets = nets(vampnet, vampnet.fetch_model())
        print(cf.on_yellow("Loaded as a VAMPNet..."))
        return Nets #namedtuple!
    except Exception as e:
        print(e)
    finally:
        print("Done!")

def get_vampnet_markov(self, generated_dcd: str):
    dataset = dl.Dataset(self.args)
    data, species, bond, ref = dataset() #data is a torch Tensor of BL3
    data = data.dataset[0] #data is a Dataset instance originally...numpy!

    #data_flatten = data.view(data.size(0), -1).contiguous().detach().cpu().numpy() #(B,3L) of raw data ... Pyemma/vampnet raw coords.
    data_flatten = data.reshape(data.shape[0], -1) #(B,3L) of raw data ... Pyemma/vampnet raw coords.
    self.model.get_hooks() 
    self.model(torch.from_numpy(data).to(self.device)) #For forward hook
    handle, fhook = getattr(self.model, "handle"), getattr(self.model, "hookf_dict")
    latent = torch.nn.functional.normalize(fhook["latent"].contiguous().detach().sum(dim=1), dim=-1).cpu().numpy() #B,100; normalized

    dihedrals = markov.plot_dihedral_ADP(self.args, self.model, self.dmo, generated_dcd) #Save png fig of FES of original and Langevin sampling w.r.t. CVs

    if not os.path.isfile(os.path.join(self.args.load_model_directory, "pyemma_vampnet.pt")) or not os.path.isfile(os.path.join(self.args.load_model_directory, "ebm_vampnet.pt")):
        print("One of the vampnet files do not exist...\n")
        print("Training...\n")
        vnet_example, vnet_ebm = list(map(functools.partial(markov.apply_vamp_clustering, lagtime=1, meta_n=6, batch_size=10000), (data_flatten, latent), (os.path.join(self.args.load_model_directory, "pyemma_vampnet.pt"), os.path.join(self.args.load_model_directory, "ebm_vampnet.pt"))  )) #Fetched vampnet namdedtuples
    else:
        vnet_example = markov.load_trained_vampnet(os.path.join(self.args.load_model_directory, "pyemma_vampnet.pt"), data=data_flatten, meta_n=6) #Fetched vampnet namdedtuples
        vnet_ebm = markov.load_trained_vampnet(os.path.join(self.args.load_model_directory, "ebm_vampnet.pt"), data=latent, meta_n=6) #Fetched vampnet namdedtuples

    datas = [data_flatten, latent]
    nets = [vnet_example, vnet_ebm]
    titles = ["example", "ebm"]
    try:
        [markov.vampnet_analysis(data=data, dihedrals=dihedrals, nets=net, lagtimes=np.arange(1,10), title=title, root=self.args.load_model_directory) for title, data, net in zip(titles, datas, nets)] 
    except Exception as e:
        print("Pass")
    return nets