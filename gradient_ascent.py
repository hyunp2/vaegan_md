https://gitlab.com/hyunp2/generative_drug/-/blob/main/relso_dynamic_ae/optim_algs.py

############################
# Gradient Methods
############################

def grad_ascent(initial_embedding, model, N_steps, lr, cycle=False,
                 noise=False, sigma=0.001):
    
    #need to pass the sequence through the network layers for gradient to be taken, so cycle the embedding once
    model.requires_grad_(True)
    grad_list = []
    
    
    # data logging
    embed_dim = initial_embedding.shape[-1]
    out_embedding_array = np.zeros((N_steps, embed_dim))
    out_fit_array = np.zeros((N_steps))
    
    # initial step
    curr_embedding = torch.tensor(initial_embedding, requires_grad=True).reshape(-1, embed_dim)
    curr_fit = model.regressor_module(curr_embedding)
    
    #print("starting fitness: {}".format(curr_fit))

    # save step 0 info
    out_embedding_array[0] = curr_embedding.reshape(1,embed_dim).detach().numpy()
    out_fit_array[0] = curr_fit.detach().numpy()
    
    assert curr_embedding.requires_grad

    for step in tqdm(range(1,N_steps)):
        model.train()
        
        grad = torch.autograd.grad(curr_fit, curr_embedding)[0] # get gradient

        if noise:
            grad += torch.normal(torch.zeros(grad.size()),sigma)     

        grad_list.append(grad.detach())

        # curr_embedding = curr_embedding.detach()
        # update step
        update_step = grad * lr
        curr_embedding = curr_embedding +  update_step 
        
        # cycle bool
        model = model.eval()
        
        if cycle:
            nseq = model.decode(curr_embedding).argmax(1)
            curr_embedding = model.encode(nseq)

        curr_fit = model.regressor_module(curr_embedding)
        
        # save step i info
        out_embedding_array[step] = curr_embedding.detach().numpy()
        out_fit_array[step] = curr_fit.detach().numpy()
        
    return out_embedding_array, out_fit_array
