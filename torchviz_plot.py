from torchviz import make_dot
import torch
import os

if __name__ == "__main__":
    from main import data_and_model, get_args

    args = get_args()

    val_dataloaders, test_dataloaders, model, resume_ckpt, args, datamodule = data_and_model(args)

    vae = model
    data = next(iter(test_dataloaders))

    z, mu, logstd, x = vae(data)  
    make_dot(x, show_attrs=True, show_saved=True, params=dict(list(vae.named_parameters()))).render(os.path.join(args.load_model_directory, "vae_viz"), format="png")
