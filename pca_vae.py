import torch
import einops
from einops.layers.torch import Rearrange
import itertools
import numpy as np
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
import os
import MDAnalysis as mda
import warnings
import gc
import inspect
import curtsies.fmtfuncs as fmt
from typing import *
import copy
import pytorch_lightning as pl
from MDAnalysis.analysis import align
from MDAnalysis.coordinates.memory import MemoryReader
from MDAnalysis.analysis.base import AnalysisFromFunction
import argparse
import dataloader as dl 
from loss_utils import *
from wasserstein_utils import wae_mmd_gaussianprior as wprior
from pca import PCA_torch

warnings.simplefilter("ignore")

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
torch.set_printoptions(precision=4)

##################################
class Encoder(torch.nn.Module):
    def __init__(self, hidden_dims=[32, 16, 8, 4, 4], **kwargs):
        super().__init__()
        self.hidden_dims = hidden_dims
        self.unrolled_dim = kwargs.get("unrolled_dim") #xyz coord dim of original protein trajectory
        self.n_components = kwargs.get("n_components") ##

        linears = torch.nn.Sequential(*[ 
                                      torch.nn.Linear(self.n_components, self.hidden_dims[0]), torch.nn.ReLU(True), 
                                      torch.nn.Linear(self.hidden_dims[0], self.hidden_dims[1]), torch.nn.ReLU(True),                 
                                      torch.nn.Linear(self.hidden_dims[1], self.hidden_dims[2]), torch.nn.ReLU(True),
                                      torch.nn.Linear(self.hidden_dims[2], self.hidden_dims[3]), torch.nn.ReLU(True),                
                                      torch.nn.Linear(self.hidden_dims[3], self.hidden_dims[4]),                     
                                    ]) #B,2

        # linears = torch.nn.Sequential(*[ 
        #                               torch.nn.Linear(self.n_components, self.hidden_dims[0]), torch.nn.ReLU(True),                     
        #                             ]) #B,2        
        self.add_module("linears_sequential", linears)

    def forward(self, inputs):
        sizes = inputs.size()
        x = inputs #BLC -> Cartesian coords...
        x = x.view(sizes[0], -1)
        x = self.linears_sequential(x)
        mu, logstd = torch.chunk(x, 2, dim=-1)
        z = self.reparameterize(mu, logstd)
        return z, mu, logstd

    def reparameterize(self, mu, logstd):
        shapes = mu.shape
        return mu + logstd.exp() * torch.distributions.Normal(0., 0.1).rsample((shapes)).to(mu)

class Decoder(torch.nn.Module):
    def __init__(self, hidden_dims=list(reversed([2, 4, 8, 16, 32])), **kwargs):
        super().__init__()
        self.hidden_dims = hidden_dims
        self.unrolled_dim = kwargs.get("unrolled_dim") #xyz coord dim of original protein trajectory
        self.n_components = kwargs.get("n_components") ##

        linears = torch.nn.Sequential(*[ 
                                      torch.nn.Linear(self.hidden_dims[0], self.hidden_dims[1]), torch.nn.ReLU(True),                 
                                      torch.nn.Linear(self.hidden_dims[1], self.hidden_dims[2]), torch.nn.ReLU(True),
                                      torch.nn.Linear(self.hidden_dims[2], self.hidden_dims[3]), torch.nn.ReLU(True),                
                                      torch.nn.Linear(self.hidden_dims[3], self.hidden_dims[4]), torch.nn.ReLU(True),           
                                      torch.nn.Linear(self.hidden_dims[4], self.n_components)
                                    ]) #B,C,H,W

        # linears = torch.nn.Sequential(*[           
        #                               torch.nn.Linear(self.hidden_dims[4], self.n_components)
        #                             ]) #B,C,H,W                                    
        self.add_module("linears_sequential", linears)


    def forward(self, inputs: "BD"):
        sizes = (1, self.unrolled_dim//3, 3) #1,L,3
        x = inputs #Latent dim
        x = self.linears_sequential(x)
        x_q = x
        #x_q = x.view(x.size(0), sizes[1], sizes[2]) #+ pos_emb #B,L,3
        return x_q #, attns


class PCA_VAE(torch.nn.Module):
    #VAE github: https://github.com/AntixK/PyTorch-VAE/tree/master/models
    """Input and output are both (B,L,3) and flattend inside Encoder/Decoder!"""
    def __init__(self, args: argparse.ArgumentParser, **kwargs):
        super().__init__()
        self.args = args
        self.hidden_dims_enc = kwargs.get("hidden_dims_enc", None)
        self.hidden_dims_dec = kwargs.get("hidden_dims_dec", None)
        self.unrolled_dim = kwargs.get("unrolled_dim", None) #xyz coord dim of original protein trajectory
        self.n_components = kwargs.get("n_components", 64) ##
    
        self.encoder = Encoder(hidden_dims=self.hidden_dims_enc, unrolled_dim=self.unrolled_dim, n_components = self.n_components)
        self.decoder = Decoder(hidden_dims=self.hidden_dims_dec, unrolled_dim=self.unrolled_dim, n_components = self.n_components)
        #self.layernorm = torch.nn.LayerNorm(self.n_components)

        self.reset_all_weights()
    
    def forward(self, inputs: "Trajectory"):
        args = self.args
        x = inputs #Normalized input
        
        config = dict(n_components=self.n_components) ##
        pca = PCA_torch(**config) ##
        ckpt = torch.load(os.path.join(args.load_pcamodel_directory, args.load_pcamodel_checkpoint + ".pt"))
        # pca_mean = ckpt.pop("pca_mean")
        # pca_std = ckpt.pop("pca_std")

        for (key, _) in pca.named_buffers():
             setattr(pca, key, ckpt[key])    
    
        pca.to(x) ##
        INS = x
        x = pca.transform(x) ##   
        INT = x         
        mean, std = pca.pca_mean, pca.pca_std
        #std = std*5
        x = (x - mean)/std
        #x = self.layernorm(x)    #normalized PCA
        print("Enc", x.mean(0), x.std(0))
        z, mu, logstd = self.encoder(x)
        print(f"encoder dim: {self.hidden_dims_enc}")
        x = self.decoder(z) #BL3, Dict: BHLL x Layers
        print("Dec", x.mean(0), x.std(0))
        x = x*std + mean
        x = pca.inverse_transform(x) 
        OUTS = pca.inverse_transform(INT) 
        print("PC loss",torch.nn.MSELoss(reduction="none")(OUTS, INS).mean(dim=(1,2)))
        # print(x.is_leaf, x.grad_fn, x.size())
        return z, mu, logstd, x
    
    #@staticmethod
    def losses(self, inputs, z, mu, logstd, recon: "x", beta, mean, std):
        assert self.args.which_model in ["fc", "pca_vae", "fc_wass"], "model must be fc or fc_wass to use this loss"
        
        mse = torch.nn.MSELoss(reduction="none")(recon, inputs).mean(dim=(1,2)) # -> (B,)
        kl = torch.sum(-0.5 * beta * (1 + logstd - mu ** 2 - logstd.exp()), dim = 1)  #kl-div (NOT a LOSS yet!); -> (B,)
        if self.args.which_model == 'fc':
            wass = kl.new_zeros(kl.size())
        if self.args.which_model == 'fc_wass':    
            wass = wprior(z, method='rf', sigma = 7)

        if 0:
            unnormalize = dl.ProteinDataset.unnormalize #static method
            original_unscaled = unnormalize(inputs, mean=mean, std=std) #BL3
            recon_unscaled = unnormalize(recon, mean=mean, std=std)
            X = original_unscaled.permute(0,2,1) #B3L
            Y = recon_unscaled.permute(0,2,1) #B3L

            X = original_unscaled #->B3L
            Y = recon_unscaled #->B3L
            X, Y = kabsch_torch(X, Y) #-> (B,3,L) alignment: translated and rotated to fit!

            rmsd = rmsd_torch(X, Y) # -> (B,)
            gdt = gdt_torch(X, Y, torch.arange(0.5, 10.5, 0.5).to(X)) # -> (B,)
            tm = tmscore_torch(X, Y) # -> (B,)

        rmsd = kl.new_zeros(kl.size())
        gdt = kl.new_zeros(kl.size())
        tm = kl.new_zeros(kl.size())
        wass = kl.new_zeros(kl.size())

        assert mse.size(0) == kl.size(0) and mse.ndim == kl.ndim and mse.ndim == 1, "all criteria for shape must match"
        assert mse.size(0) == rmsd.size(0) and mse.size(0) == gdt.size(0) and mse.size(0) == tm.size(0), "all criteria for shape must match"
        return mse, kl, wass, rmsd, gdt, tm

    def reset_all_weights(self, ) -> None:
        """
        refs:
        - https://discuss.pytorch.org/t/how-to-re-set-alll-parameters-in-a-network/20819/6
        - https://stackoverflow.com/questions/63627997/reset-parameters-of-a-neural-network-in-pytorch
        - https://pytorch.org/docs/stable/generated/torch.nn.Module.html
        """

        @torch.no_grad()
        def weight_reset(m: torch.nn.Module):
             # - check if the current module has reset_parameters & if it's callabed called it on m
            reset_parameters = getattr(m, "reset_parameters", None)
            if callable(reset_parameters):
                m.reset_parameters()

        # Applies fn recursively to every submodule see: https://pytorch.org/docs/stable/generated/torch.nn.Module.html
        self.apply(fn=weight_reset)

