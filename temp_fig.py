
import torch, torchani
# import transformers as tfm
# import dataloader as dl 
# import physnet_block as pb
import pytorch_lightning as pl
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
# import MDAnalysis as mda
import tqdm
import pickle, json, os, copy
import gc
import collections
import networkx as nx
import yaml
import logging
import warnings
# import wandb
# from vae import VAE
# from convVae import VanillaVAE
# from twoStageVae import TwoStageVAE
# from vaeNeuralODE import NeuralVAE as nVAE
import argparse
from typing import *
from loss_utils import *
from curtsies import fmtfuncs as cf
import io, PIL
from plotly.subplots import make_subplots
import plotly.graph_objs as go

#pl.seed_everything(42)

#WIP for PCA or UMAP or MDS
import plotly.express as px
import scipy.stats

save_data_directory = "/Scr/yifei6/vaegan_md/sample_output"


data    = np.load(os.path.join(save_data_directory, 'manifold_latent.npz'))
mus     = data['mus']
logstds = data['logstds']
##### figure in plot_manifold
# np.save(os.path.join(save_data_directory, f"epoch_{title}_latent.npy"), mus)
assert len(mus.shape) == 2 and len(logstds.shape) == 2
path_to_plotly_html = os.path.join(save_data_directory, "plotly_figure.html")
dist = scipy.stats.multivariate_normal(np.zeros(mus.shape[1]), 1)

colors = [mus, logstds, np.arange(mus.shape[0])]

if mus.shape[1] == 2:
    for i, c in enumerate(colors):
        if i == 0:
            fig = px.scatter(x=mus[:,0], y=mus[:,1], color=dist.pdf(c).reshape(-1,)) 
        elif i == 1:
            fig = px.scatter(x=mus[:,0], y=mus[:,1], color=np.exp(c.sum(axis=-1)).reshape(-1,)) 
        elif i == 2:
            fig = px.scatter(x=mus[:,0], y=mus[:,1], color=c.reshape(-1,)) 
        fig.write_html(path_to_plotly_html, auto_play = False)
        fig.write_image(os.path.splitext(path_to_plotly_html)[0] + f"_labels_{i}.png")

elif mus.shape[1] >= 3:
    for i, c in enumerate(colors):
        if i == 0:
            fig = px.scatter_3d(x=mus[:,0], y=mus[:,1], z=mus[:,2], color=dist.pdf(c).reshape(-1,)) 
        elif i == 1:
            fig = px.scatter_3d(x=mus[:,0], y=mus[:,1], z=mus[:,2], color=np.exp(c.sum(axis=-1)).reshape(-1,)) 
        elif i == 2:
            fig = px.scatter_3d(x=mus[:,0], y=mus[:,1], z=mus[:,2], color=c.reshape(-1,)) 
        fig.write_html(path_to_plotly_html, auto_play = False)
        fig.write_image(os.path.splitext(path_to_plotly_html)[0] + f"_logprob_{i}.png")


############# figure in analyze_molecule
### Bar plot

data    = np.load(os.path.join(save_data_directory, "analyze_saved_bar.npz"))
points = data['points']
fig, ax   = plt.subplots(3, 3, figsize=(10,10), sharex='col')
point_dict = collections.defaultdict(list)
#print(data['points'])
# np.savez(os.path.join(self.args.save_data_directory, "analyze_saved_bar.npz"), points=points)
for i, (p, n, t) in enumerate(zip(points, ["rmsd","gdt","tm"]*3, np.repeat(np.array(["Original vs Reconstruction", "Reference vs Original", "Reference vs Reconstruction"]), 3, axis=0) )):
    ax.flatten()[i].hist(p, bins=15, density=True)
    if i >= 6:
        ax.flatten()[i].set_xlabel(n)
    if i % 3 == 0:
        ax.flatten()[i].set_ylabel("Density")            
    if (i-1) % 3 == 0 :
        ax.flatten()[i].set_title(t)
    if i % 3 == 0:
        point_dict["rmsd_mean"].append(p.mean())  
        point_dict["rmsd_std"].append(p.std())  
    if (i - 1) % 3 == 0:
        point_dict["gdt_mean"].append(p.mean())  
        point_dict["gdt_std"].append(p.std())  
    if (i - 2) % 3 == 0:
        point_dict["tm_mean"].append(p.mean())  
        point_dict["tm_std"].append(p.std())  
fig.savefig(os.path.join(save_data_directory, "analyze_saved_bar.png"))
print("Bar plot done ....")



data        = np.load(os.path.join(save_data_directory, 'analyze_saved.npz'))
mesh_inputs = data['mesh']
mus_orig    = data['mus']
probs       = data['prob']
rmsds       = data['rmsd']
gdts        = data['gdt']
tms         = data['tm']
deformation = data['deform']
mesh_inputs, mus_orig, probs, rmsds, gdts, tms, deformation \
= list(map(lambda inp: torch.from_numpy(inp), \
[mesh_inputs, mus_orig, probs, rmsds, gdts, tms, deformation])) 

npoints = mesh_inputs.size(1)

# np.savez(os.path.join(self.args.save_data_directory, "analyze_saved.npz"), 
# mesh=mesh_inputs.detach().cpu().numpy(), 
# mus=mus_orig.detach().cpu().numpy(), 
# prob = probs.detach().cpu().numpy(), 
# rmsd = rmsds.detach().cpu().numpy(), 
# gdt = gdts.detach().cpu().numpy(), 
# tm= tms.detach().cpu().numpy(),  
# deform = deformation.detach().cpu().numpy())

### 2D latent space plot
from plotly.subplots import make_subplots
import plotly.graph_objs as go
fig = make_subplots(rows=3, cols=2, subplot_titles=('Log Prob','RMSD','GDT','TM', "Deformation"))
fig.add_trace(go.Contour(x=mesh_inputs[0].detach().cpu().numpy(), y=mesh_inputs[1].detach().cpu().numpy(), z=probs.detach().cpu().numpy().reshape(npoints, npoints), showscale=False,
                            contours=dict(
                            coloring ='heatmap',
                            showlabels = True, # show labels on contours
                            labelfont = dict( # label font properties
                                size = 12,
                                color = 'white',
                            ))
                            ), 1, 1)      
fig.add_trace(go.Scatter(x=mus_orig[:,0].detach().cpu().numpy(), y=mus_orig[:,1].detach().cpu().numpy(), mode="markers"), 1, 1)
fig.add_trace(go.Contour(x=mesh_inputs[0].detach().cpu().numpy(), y=mesh_inputs[1].detach().cpu().numpy(), z=rmsds.detach().cpu().numpy().reshape(npoints, npoints), showscale=False, 
                            contours=dict(
                            coloring ='heatmap',
                            showlabels = True, # show labels on contours
                            labelfont = dict( # label font properties
                                size = 12,
                                color = 'white',
                            ))
                            ), 1, 2)    
fig.add_trace(go.Scatter(x=mus_orig[:,0].detach().cpu().numpy(), y=mus_orig[:,1].detach().cpu().numpy(), mode="markers"), 1, 2)
fig.add_trace(go.Contour(x=mesh_inputs[0].detach().cpu().numpy(), y=mesh_inputs[1].detach().cpu().numpy(), z=gdts.detach().cpu().numpy().reshape(npoints, npoints), showscale=False,
                            contours=dict(
                            coloring ='heatmap',
                            showlabels = True, # show labels on contours
                            labelfont = dict( # label font properties
                                size = 12,
                                color = 'white',
                            ))
                            ), 2, 1)    
fig.add_trace(go.Scatter(x=mus_orig[:,0].detach().cpu().numpy(), y=mus_orig[:,1].detach().cpu().numpy(), mode="markers"), 2, 1)
fig.add_trace(go.Contour(x=mesh_inputs[0].detach().cpu().numpy(), y=mesh_inputs[1].detach().cpu().numpy(), z=tms.detach().cpu().numpy().reshape(npoints, npoints), showscale=False,
                            contours=dict(
                            coloring ='heatmap',
                            showlabels = True, # show labels on contours
                            labelfont = dict( # label font properties
                                size = 12,
                                color = 'white',
                            ))
                            ), 2, 2)    
fig.add_trace(go.Scatter(x=mus_orig[:,0].detach().cpu().numpy(), y=mus_orig[:,1].detach().cpu().numpy(), mode="markers"), 2, 2)
fig.add_trace(go.Contour(x=mesh_inputs[0].detach().cpu().numpy(), y=mesh_inputs[1].detach().cpu().numpy(), z=deformation.detach().cpu().numpy().reshape(npoints, npoints), showscale=False,
                            contours=dict(
                            coloring ='heatmap',
                            showlabels = True, # show labels on contours
                            labelfont = dict( # label font properties
                                size = 12,
                                color = 'white',
                            ))
                            ), 3, 1)    
fig.add_trace(go.Scatter(x=mus_orig[:,0].detach().cpu().numpy(), y=mus_orig[:,1].detach().cpu().numpy(), mode="markers"), 3, 1)

path_html = "plotly_visualization_output.html" #overwrite is ok...
fig.write_html(path_html, auto_play = False)
path_to_plotly_html = os.path.join(save_data_directory, "plotly_visualization_output.html")
fig.write_image(os.path.splitext(path_to_plotly_html)[0] + "_subplots.png")
print("2D plot done ....")


        

