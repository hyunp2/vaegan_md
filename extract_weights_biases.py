#!/usr/bin/env python3
import numpy as np
# from tensorflow.keras.models import model_from_json
import torch
import argparse


def load_model(model_filename):
    '''load tensorflow model from saved files'''
    ckpt: dict = torch.load(model_filename, map_location=torch.device("cpu"))["state_dict"]
    return ckpt

def dump_dense_weights_biases(ckpt: dict):
    for index, (key, param) in enumerate(ckpt.items()):
        if 'encoder' in key and 'weight' in key:
            if not '8' in key:
                np.savetxt(f'dense_{index//2+1}_weights.txt', param.detach().cpu().numpy())       
            else:
                np.savetxt(f'dense_{index//2+1}_weights.txt', torch.chunk(param, 2, dim=0)[0].detach().cpu().numpy())       
        elif 'encoder' in key and 'bias' in key
            if not '8' in key:
                np.savetxt(f'dense_{index//2+1}_biases.txt', param.detach().cpu().numpy())
            else:
                np.savetxt(f'dense_{index//2+1}_biases.txt', torch.chunk(param, 2, dim=0)[0].detach().cpu().numpy())   

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('model_file', type=str, help='model file of the tensorflow trained model (JSON format)')
    parser.add_argument('weights_file', type=str, help='weight file of the tensorflow trained model (HDF5 format)')
    args = parser.parse_args()
    ckpt = load_model(args.model_file)
    dump_dense_weights_biases(ckpt)