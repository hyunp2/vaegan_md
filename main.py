import torch
import einops
from einops.layers.torch import Rearrange
import itertools
import numpy as np
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE
import os
import MDAnalysis as mda
import warnings
import gc
import inspect
import curtsies.fmtfuncs as fmt
from typing import *
import copy
import pytorch_lightning as pl
from MDAnalysis.analysis import align
from MDAnalysis.coordinates.memory import MemoryReader
from MDAnalysis.analysis.base import AnalysisFromFunction
import argparse
from vaeLightning import Model
import dataloader as dl 
from util import plot_dssp
import curtsies.fmtfuncs as cf

#/Scr/hyunpark/anaconda3/envs/deeplearning

def get_args():
    parser = argparse.ArgumentParser(description='Training')

    parser.add_argument('--user', type=str, default="hyun", choices=['none', 'hyun', 'yifei'], help='user name for weight bias and directory...')  

    #Model related
    parser.add_argument('--load_model_directory', "-dirm", type=str, default="/Scr/hyunpark/Monster/vaegan_md_gitlab/output", help='Model ROOT directory...')  
    parser.add_argument('--load_model_checkpoint', "-ckpt", type=str, default=None, help='Find NAME of a CHECKPOINT')  
    parser.add_argument('--name', type=str, default=None, help='Name for Wandb and GENERATED data!')  
    parser.add_argument('--standard_file', "-sf", type=str, default="3f48final_standard.npy", help='A file containing mean and std of trained dataset')  
    parser.add_argument('--standard_file_distogram', type=str, default="3f48final_standard_distogram.npy", help='Distances: A file containing mean and std of trained dataset')  
    parser.add_argument('--distogram', action="store_true", help="Use distogram based Dataloader")  
    parser.add_argument('--which_model', type=str, default="vanilla", choices=["fc", "vanilla", "twostage", "neural", "pca", "fc_ae","fc_wass","pca_vae"], help="Use distogram based Convolutions")  

    #Molecule (Dataloader) related
    parser.add_argument('--load_data_directory', "-dird", default="/Scr/hyunpark/Monster/vaegan_md_gitlab/data", help='Locate ORIGINAL data')  
    parser.add_argument('--save_data_directory', "-sird", default="/Scr/hyunpark/Monster/vaegan_md_gitlab/generated_data", help='Save GENERATED data')  
    parser.add_argument('--psf_file', '-psf', type=str, default=None, help='MDAnalysis PSF')
    parser.add_argument('--pdb_file', '-pdb', type=str, default=None, help='MDAnalysis PDB')
    parser.add_argument('--trajectory_files', '-traj', type=str, nargs='*', default=None, help='MDAnalysis Trajectories')
    parser.add_argument('--dssp_original_trajs', type=str, nargs='*', default=None, help='MDAnalysis Orig Test Trajectories')
    parser.add_argument('--dssp_recon_trajs', type=str, nargs='*', default=None, help='MDAnalysis Recon of Test Trajectories')
    parser.add_argument('--atom_selection', '-asel', type=str, default="(protein and backbone) or (resname SOD CLA)", help='MDAnalysis atom selection')
    parser.add_argument('--align_selection', '-alsel', type=str, default="protein and backbone", help='MDAnalysis align selection')
    parser.add_argument('--split_portion', '-spl', type=int, nargs="*", help='Torch dataloader and Pytorch lightning split of batches')
    parser.add_argument('--split_method', '-spl_m', type=str, default= 'end', choices=['middle', 'end'], help=' end: testing with data in the end; middle: testing with data in the middle')   
    parser.add_argument('--truncate', type=int, default=0, help='Window size for distogram')
    parser.add_argument('--dssp_trajectories', type=str, nargs='*', default=None, help='MDAnalysis DSSP Trajectories analysis')
    parser.add_argument('--dssp_names', type=str, nargs='*', default=None, help='MDAnalysis DSSP Trajectories analysis names')

    #Optimizer related
    parser.add_argument('--num_epochs', default=100, type=int, help='number of epochs')
    parser.add_argument('--batch_size', '-b', default=128, type=int, help='batch size')
    parser.add_argument('--lr', default=1e-2, type=float, help='learning rate')
    parser.add_argument('--ngpus', type=int, default=-1, help='Number of GPUs, -1 use all available. Use CUDA_VISIBLE_DEVICES=1, to decide gpus')
    parser.add_argument('--num_nodes', type=int, default=1, help='Number of nodes')
    parser.add_argument('--warm_up_split', type=int, default=5, help='warmup times')
    parser.add_argument('--scheduler', type=str, default="cosine", help='scheduler type')
    parser.add_argument('--max_epochs', default=60, type=int, help='number of epochs max')
    parser.add_argument('--min_epochs', default=1, type=int, help='number of epochs min')
    parser.add_argument('--precision', type=int, default=32, choices=[16, 32], help='Floating point precision')
    parser.add_argument('--monitor', type=str, default="epoch_val_loss", help='metric to watch')
    parser.add_argument('--save_top_k', type=int, default="5", help='num of models to save')
    parser.add_argument('--patience', type=int, default=10, help='patience for stopping')
    parser.add_argument('--metric_mode', type=str, default="min", help='mode of monitor')
    parser.add_argument('--amp_backend', type=str, default="native", help='Torch vs NVIDIA AMP')
    parser.add_argument('--sanity_checks', '-sc', type=int, default=2, help='Num sanity checks..')
    parser.add_argument('--accelerator', "-accl", type=str, default="gpu", help='accelerator type', choices=["cpu","gpu","tpu"])
    parser.add_argument('--strategy', "-st", default="ddp", help='accelerator type', choices=["ddp_spawn","ddp","dp","ddp2","horovod","none"])
    parser.add_argument('--gradient_clip', "-gclip", default=1., type=float, help='norm clip value')
    parser.add_argument('--gradient_accm', "-gaccm", default=1, type=int, help='accm value')

    #Misc.
    parser.add_argument('--num_workers', type=int, default=4, help='Number of workers for data prefetch')
    parser.add_argument('--train_mode', type=str, default="train", choices=["train","test","pred","sample","sample_noise", "analyze", "dssp", "tune_loss","plot_pca","explain", "refine"])
    parser.add_argument('--seed', type=int, default=42)
    parser.add_argument('--beta', type=float, default=1., help="beta-VAE")
    parser.add_argument('--lerp', type=str, default="linear", choices=["linear","geometric"], help="beta-VAE")
    parser.add_argument('--num_interps', type=int, default=200, help='Except for start and end, interpolations')
    parser.add_argument('--sampling_noise', nargs="*", default=[0, 0.001, 0.01, 0.05, 0.1, 0.5, 1], help='standard deviation for eps in mu+logstd.exp() * eps')
    parser.add_argument('--nolog', action="store_true", help="disable logging")
    parser.add_argument('--analysis_backend', type=str, default="train", choices=["mdtraj", "mdanalysis"])
    parser.add_argument('--atom_pair_indices', nargs="*", default=[[1,2],[3,4]], help="npairs by 2 -- row/col") #https://github.com/hyunp2/Protein-TDA/blob/d8dbff5ad745c98f784cbe83b3a6b1900c6e4e18/main.py#:~:text=nargs%3D2%2C%20default%3D%5B1.%2C%201.%5D%2C%20type%3Dfloat%2C%20help%3D%22CE%20and%20Reg%20loss%20weights%22
    parser.add_argument('--atom_angle_pair_indices', nargs="*", default=[[1,2],[3,4]], help="npairs by 2 -- row/col") #https://github.com/hyunp2/Protein-TDA/blob/d8dbff5ad745c98f784cbe83b3a6b1900c6e4e18/main.py#:~:text=nargs%3D2%2C%20default%3D%5B1.%2C%201.%5D%2C%20type%3Dfloat%2C%20help%3D%22CE%20and%20Reg%20loss%20weights%22
    parser.add_argument('--atom_sasa_indices', type=str, default="(resid == 100) or (resid 103 to 105) or (resid == 175) or (resid 327 to 338) or (resid == 368) or (resid 490 to 503) or (resid 549 to 557) or (resid  561) or (resid 563) or (resid 579)", help="npairs by 2 -- row/col") 
    parser.add_argument('--n_components', type=int, default=64)
    parser.add_argument('--wbeta', type=float, default=1., help="wass loss beta")

    parser.add_argument('--load_pcamodel_directory', "-pcadirm", type=str, default="/Scr/hyunpark/Monster/vaegan_md_gitlab/output", help='Model ROOT directory...')  
    parser.add_argument('--load_pcamodel_checkpoint', "-pcackpt", type=str, default=None, help='Find NAME of a CHECKPOINT')     

    args = parser.parse_args()
    return args

def data_and_model(args):
    pl.seed_everything(args.seed)
    # ------------------------
    # 0 MAKE REDUCED PDB
    # ------------------------
    if args.which_model in ["fc", "neural", "pca", 'fc_ae',"fc_wass","pca_vae"]:
        args.distogram = False  #set distogram False
        datamodule = dl.DataModule(args) 
    elif args.which_model in ["vanilla", "twostage"]:
        args.distogram = True  #set distogram True
        datamodule = dl.DataModule(args) 
    datamodule.setup()

    # ------------------------
    # 1 INIT LIGHTNING MODEL
    # ------------------------
    atom_selection = args.atom_selection
    pdb = os.path.join(args.load_data_directory, os.path.splitext(args.pdb_file)[0] + "_reduced.pdb") #string
#     psf = os.path.join(args.load_data_directory, args.psf_file) #string
    prot_ref = mda.Universe(pdb) #PSF must not be considered
    pos = prot_ref.atoms.select_atoms(atom_selection).positions #L,3; Does not affect anymore since Datamodule already takes care of it!
    unrolled_dim = pos.shape[0] * pos.shape[1]

    if args.which_model in ["fc", "neural","fc_wass"]:        
        model_configs = dict(hidden_dims_enc=[2048, 1024, 512, 128, 128],
                            hidden_dims_dec=[64, 128, 512, 1024, 2048],
                            unrolled_dim=unrolled_dim)
    if args.which_model in ["fc_ae"]:        
        model_configs = dict(hidden_dims_enc=[2048, 1024, 512, 128, 64], # change 128 for VAE -> 64 for AE
                            hidden_dims_dec=[64, 128, 512, 1024, 2048],
                            unrolled_dim=unrolled_dim)                            
    elif args.which_model == "vanilla":
        model_configs = dict(in_channels=1,
                            latent_dim=512,
                            hidden_dims=[32, 64, 128, 256, 512])
    elif args.which_model == "twostage":
        model_configs = dict(in_channels=1,
                            latent_dim=512,
                            hidden_dims=[32, 64, 128, 256, 512],
                            hidden_dims2=[1024, 1024])
    elif args.which_model == "pca":
        model_configs = dict(full_matrices=False, n_components=args.n_components)
    elif args.which_model == "pca_vae":    
        # model_configs = dict(hidden_dims_enc=[2048, 1024, 512, 128, 128],
        #                     hidden_dims_dec=[64, 128, 512, 1024, 2048],
        #                     unrolled_dim=unrolled_dim,
        #                     n_components = args.n_components)
        model_configs = dict(hidden_dims_enc=[32, 16, 8, 4, 4],
                            hidden_dims_dec=[2, 4, 8, 16, 32],
                            unrolled_dim=unrolled_dim,
                            n_components = args.n_components)
        # model_configs = dict(hidden_dims_enc=[64*2, 16, 8, 4, 4],
        #                     hidden_dims_dec=[2, 4, 8, 16, 64],
        #                     unrolled_dim=unrolled_dim,
        #                     n_components = args.n_components)        
    args.unrolled_dim = unrolled_dim
    
    model = Model.load_from_checkpoint( os.path.join(args.load_model_directory, args.load_model_checkpoint), args=args, model_configs=model_configs, strict=True ) if args.load_model_checkpoint and not args.which_model == "pca" else Model(args=args, model_configs=model_configs)

    # ------------------------
    # MISC.
    # ------------------------
    if args.load_model_checkpoint:
        resume_ckpt = os.path.join(args.load_model_directory, args.load_model_checkpoint)
    else:
        resume_ckpt = None
        
    if args.strategy in ["none", None]:
        args.strategy = None
        args.syncbatch = False
    else:
        args.syncbatch = True
    
    train_dataloaders, val_dataloaders, test_dataloaders = datamodule.train_dataloader(), datamodule.val_dataloader(), datamodule.test_dataloader()
    [setattr(model, key, val) for key, val in zip(["data_mean", "data_std", "loader_length"], [datamodule.mean, datamodule.std, len(datamodule.dataset) ])] #set mean and std
    print("Model's dataset mean and std are set:", model.data_mean, " and ", model.data_std)
    
    # sort of pca version of model = Model.load_from_checkpoint()
    if args.which_model == "pca":
        trainset, valset = train_dataloaders.dataset.dataset, val_dataloaders.dataset.dataset
        if not os.path.isfile( os.path.join(args.load_pcamodel_directory, args.load_pcamodel_checkpoint + ".pt")) or args.train_mode == "train":
            print(cf.on_yellow(f"{args.train_mode} MODEL and SAVING PCA!!!"))
            model.model_block.fit(trainset)    
            torch.save(model.model_block.state_dict(), os.path.join(args.load_pcamodel_directory, args.load_pcamodel_checkpoint + ".pt"))
        else:
            print(cf.on_blue(f"{args.train_mode} MODEL and LOADING PCA!!!"))
            ckpt = torch.load(os.path.join(args.load_model_directory, args.load_pcamodel_checkpoint + ".pt"))
            for (key, _) in model.model_block.named_buffers():
                setattr(model.model_block, key, ckpt[key])
            
    if args.train_mode in ["train","tune_loss"]:
        return train_dataloaders, val_dataloaders, model, resume_ckpt, args, datamodule
    if args.train_mode in ["test", "pred", "sample", "sample_noise", "analyze", "explain"]:
        return val_dataloaders, test_dataloaders, model, resume_ckpt, args, datamodule
    
def _main(args):
    # args = get_args()
    train_dataloaders, val_dataloaders, model, resume_ckpt, args, datamodule = data_and_model(args)
    
    # ------------------------
    # 2 INIT EARLY STOPPING
    # ------------------------
    early_stop_callback = pl.callbacks.EarlyStopping(
        monitor=args.monitor,
        min_delta=0.0,
        patience=1000,
        verbose=True,
        mode=args.metric_mode,
    )

    # --------------------------------
    # 3 INIT MODEL CHECKPOINT CALLBACK
    #  -------------------------------
    # initialize Model Checkpoint Saver
    checkpoint_callback = pl.callbacks.ModelCheckpoint(
    filename=None,
    save_top_k=args.save_top_k,
    verbose=True,
    monitor=args.monitor,
    every_n_epochs=1,
    mode=args.metric_mode,
    dirpath=args.load_model_directory,
    )

    # --------------------------------
    # 4 INIT SWA CALLBACK
    #  -------------------------------
    # Stochastic Weight Averaging
    swa_callback = pl.callbacks.StochasticWeightAveraging(swa_epoch_start=0.8, swa_lrs=None, annealing_epochs=10, annealing_strategy='cos', avg_fn=None)

    # --------------------------------
    # 5 INIT SWA CALLBACK
    #  -------------------------------
    # Stochastic Weight Averaging
#     rsummary_callback = pl.callbacks.RichModelSummary() #Not in this PL version

    # --------------------------------
    # 6 INIT MISC CALLBACK
    #  -------------------------------
    # MISC
#     progbar_callback = pl.callbacks.ProgressBar()
    timer_callback = pl.callbacks.Timer()
    tqdmbar_callback = pl.callbacks.TQDMProgressBar()
    
    # ------------------------
    # N INIT TRAINER
    # ------------------------
    csv_logger = pl.loggers.CSVLogger(save_dir=args.load_model_directory)
#     plugins = DDPPlugin(find_unused_parameters=False) if hparams.accelerator == "ddp" else None

    if args.which_model != "pca":
        trainer = pl.Trainer(
            logger=[csv_logger],
            max_epochs=args.num_epochs,
            min_epochs=args.min_epochs,
            callbacks = [early_stop_callback, checkpoint_callback, swa_callback, tqdmbar_callback, timer_callback],
            precision=args.precision,
            amp_backend=args.amp_backend,
            deterministic=False,
            default_root_dir=args.load_model_directory,
            num_sanity_val_steps = args.sanity_checks,
            check_val_every_n_epoch=1,
            log_every_n_steps=4,
            gradient_clip_algorithm="norm",
            accumulate_grad_batches=args.gradient_accm,
            gradient_clip_val=args.gradient_clip,
            devices=args.ngpus,
            strategy=args.strategy,
            accelerator=args.accelerator,
            auto_select_gpus=True,
            resume_from_checkpoint=resume_ckpt,
            sync_batchnorm=args.syncbatch
        )
        
        for idx, callback in enumerate(trainer.callbacks):
            if isinstance(callback,  pl.callbacks.early_stopping.EarlyStopping):
                trainer.callbacks[idx].patience = args.num_epochs
            else:
                pass
    
        trainer.fit(model, train_dataloaders, val_dataloaders) #New API!
        
        model.wandb_run.finish()
        
    else:
        trainset, valset = train_dataloaders.dataset.dataset, val_dataloaders.dataset.dataset
        # model.model_block.fit(trainset)
        pca_train = model.model_block(trainset)
        pca_train_recon = model.model_block.inverse_transform(pca_train)
        pca_val = model.model_block(valset)
        pca_val_recon = model.model_block.inverse_transform(pca_val)
        model.wandb_run.summary["scree"] = model.model_block.explained_variance_ratio_.cumsum(dim=0) * 100
        print(model.model_block.explained_variance_ratio_.cumsum(dim=0)[:64] * 100)
        # model.wandb_run.summary.update()
        model.wandb_run.finish()

def _test(args: argparse.ArgumentParser):
    val_dataloaders, test_dataloaders, model, resume_ckpt, args, datamodule = data_and_model(args)
        
    csv_logger = pl.loggers.CSVLogger(save_dir=args.load_model_directory)

    trainer = pl.Trainer(
        logger=[csv_logger],
        max_epochs=args.max_epochs,
        min_epochs=args.min_epochs,
        precision=args.precision,
        amp_backend=args.amp_backend,
        deterministic=False,
        default_root_dir=args.load_model_directory,
        num_sanity_val_steps = args.sanity_checks,
        log_every_n_steps=4,
        gradient_clip_algorithm="norm",
        accumulate_grad_batches=args.gradient_accm,
        gradient_clip_val=args.gradient_clip,
        devices=args.ngpus,
        strategy=args.strategy,
        accelerator=args.accelerator,
        auto_select_gpus=True,
    )
    if args.train_mode in ["test"]:
        trainer.test(model, dataloaders=test_dataloaders, ckpt_path=resume_ckpt) #New API!
    elif args.train_mode in ["pred"]:
        warnings.warn(" keyword 'pred' is DEPRECATED... Use 'test' instead... ")
        trainer.test(model, dataloaders=test_dataloaders, ckpt_path=resume_ckpt) #New API!
#         test_dataloader = model.test_dataloader()
#         trainer.predict(model, dataloaders=test_dataloaders, ckpt_path=resume_ckpt)

def _explain(args: argparse.ArgumentParser):
    # args.batch_size = 4
    _, test_dataloaders, model, resume_ckpt, args, datamodule = data_and_model(args)
    # args.truncate = datamodule.reference.size(1) #give it length of atoms; somehow truncate = 0 does not work!

    # input_coords = iter(test_dataloaders).next() #Will be Shuffled although!!!
    assert args.train_mode == "explain"
    test_data = torch.cat([test_batch for test_batch in test_dataloaders]) #[(B, Dim=3*N)] -> (Bs, Dim)
    model.saliency(test_data) #Generate recon and interp

def _refine(args: argparse.ArgumentParser):
    assert args.train_mode == "refine"
    model.refine #property: Generate DCD with FULL-ATOMS!

def _sample(args: argparse.ArgumentParser):
    # args.batch_size = 4
    _, test_dataloaders, model, resume_ckpt, args, datamodule = data_and_model(args)
    # args.truncate = datamodule.reference.size(1) #give it length of atoms; somehow truncate = 0 does not work!

    # input_coords = iter(test_dataloaders).next() #Will be Shuffled although!!!
    if args.which_model == "pca":
        model.generate_molecules_pca(test_dataloaders, 0, -1, args.num_interps, datamodule.reference, min=datamodule.min, max=datamodule.max) #Generate recon and interp
    else: 
        model.generate_molecules(test_dataloaders, 0, -1, args.num_interps, datamodule.reference, min=datamodule.min, max=datamodule.max) #Generate recon and interp
        
def _sample_noise(args: argparse.ArgumentParser):
    # args.batch_size = 4
    _, test_dataloaders, model, resume_ckpt, args, datamodule = data_and_model(args)
    # args.truncate = datamodule.reference.size(1) #give it length of atoms; somehow truncate = 0 does not work!

    # input_coords = iter(test_dataloaders).next() #Will be Shuffled although!!!
    model.generate_molecules_with_noise(test_dataloaders, 0, -1, args.num_interps, datamodule.reference, min=datamodule.min, max=datamodule.max) #Generate recon and interp

def _analyze(args: argparse.ArgumentParser):
    _, test_dataloaders, model, resume_ckpt, args, datamodule = data_and_model(args)
    model.analyze_molecule(test_dataloaders, 0, -1, args.num_interps, datamodule.reference, min=datamodule.min, max=datamodule.max) #Generate recon and interp

def _dssp(args: argparse.ArgumentParser):
    # _, test_dataloaders, model, resume_ckpt, args, datamodule = data_and_model(args)
    # dssp = model.calc_dssp

    import mdtraj as md
    pdb = os.path.join(args.load_data_directory, os.path.splitext(args.pdb_file)[0] + "_reduced.pdb") #string
    dssp_trajs = list(map(lambda inp: os.path.join(args.save_data_directory, inp), args.dssp_trajectories ))
    dssp_names = args.dssp_names
    assert len(dssp_trajs) == len(dssp_names), "Lengths do not match!"
    dssp_instances = zip(dssp_trajs, dssp_names)
    # import collections
    dssp_dict = dict()
    for dssp_traj, dssp_name in dssp_instances:
        traj = md.load(dssp_traj, top=pdb)
        dssp_dict[dssp_name] = md.compute_dssp(traj, simplified=True)
    plot_dssp(args, dssp_dict)    
    
    np.savez(os.path.join(args.save_data_directory, f"dssp_{args.name}.npz"), **dssp_dict)   
    # dssp_original_trajs = os.path.join(args.save_data_directory, args.dssp_original_trajs[0])
    # dssp_recon_trajs = os.path.join(args.save_data_directory, args.dssp_recon_trajs[0])
    # pdb = os.path.join(args.load_data_directory, os.path.splitext(args.pdb_file)[0] + "_reduced.pdb") #string
    # traj_orig = md.load(dssp_original_trajs, top=pdb)
    # traj_recon = md.load(dssp_recon_trajs, top=pdb)
    # dssp_orig = md.compute_dssp(traj_orig, simplified=True) #(nframes, nres)
    # dssp_recon = md.compute_dssp(traj_recon, simplified=True) #(nframes, nres)
    # np.savez(os.path.join(args.save_data_directory, f"analyze_saved_dssp_{args.name}.npz"), dssp_orig = dssp_orig, dssp_recon = dssp_recon)    

def _compare_kl_wass_mse(args: argparse.ArgumentParser):
    # args.num_epochs = 1000 
    # betas = np.linspace(0, 2, num=5)
    # name = args.name
    # for i, beta in enumerate(betas):
    #     args.beta = beta
    #     args.name = f"{name}_loss_tune_beta_{i}"
    #     _main(args)

    args.num_epochs = 1000 
    wbetas = np.linspace(0.5, 2, num=4)
    name = args.name
    for i, wbeta in enumerate(wbetas):
        args.wbeta = wbeta
        args.name = f"{name}_loss_tune_wass_{i}"
        _main(args)


def _plot_pca(args: argparse.ArgumentParser):
    train_dataloaders, val_dataloaders, model, resume_ckpt, args, datamodule = data_and_model(args) 


    trainset, valset = train_dataloaders.dataset.dataset, val_dataloaders.dataset.dataset
    # model.model_block.fit(trainset)
    pca_train = model.model_block(trainset)
    #pca_train_recon = model.model_block.inverse_transform(pca_train)
    pca_val = model.model_block(valset)
    #pca_val_recon = model.model_block.inverse_transform(pca_val)
    model.wandb_run.summary["scree"] = model.model_block.explained_variance_ratio_.cumsum(dim=0) * 100
    print(model.model_block.explained_variance_ratio_.cumsum(dim=0)[:64] * 100)
    # model.wandb_run.summary.update()

    model.wandb_run.finish()


if __name__ == "__main__":
    args = get_args()
    
    if args.user == "yifei":
        args.load_model_directory = "/Scr/yifei6/vaegan_md/" + args.load_model_directory
        args.load_data_directory = "/Scr/yifei6/vaegan_md/data"
        args.save_data_directory = "/Scr/yifei6/vaegan_md/sample_output"
        # args.save_data_directory = "/Scr/yifei6/vaegan_md/analysis_files"

    print(args.__dict__)
    
    if args.train_mode in ["train"]:
        _main(args)
    elif args.train_mode in ["test"]:
        _test(args)
    elif args.train_mode in ["pred"]:
        _test(args)
    elif args.train_mode in ["sample"]:
        _sample(args)
    elif args.train_mode in ["sample_noise"]:
        _sample_noise(args)
    elif args.train_mode in ["analyze"]:
        _analyze(args)
        _dssp(args) # secondary structure analyzes algorithm need to fix this 
    elif args.train_mode in ["tune_loss"]:    
        _compare_kl_wass_mse(args)
    elif args.train_mode in ["plot_pca"]:  
        _plot_pca(args)
    elif args.train_mode in ["explain"]:  
        _explain(args)
    elif args.train_mode in ["refine"]:  
        _refine(args)

    # elif args.train_mode in ["dssp"]: # secondary structure analyzes algorithm 
    #     _dssp(args)

#     python -m main --psf_file 3f48final.psf --pdb_file 3f48finaleqnbfix.pdb --trajectory_files force5tm18_afterreleaseeq_nbfix.dcd --align_selection backbone --atom_selection backbone --strategy none --batch_size 2048 --scheduler reduce -gaccm 1 --train_mode train --num_epochs 15000 --lr 0.001 -ckpt epoch=9917-step=19836.ckpt --which_model fc --split_portion 94

# python -m main --psf_file 3f48final.psf --pdb_file 3f48finaleqnbfix.pdb --trajectory_files force5tm18_afterreleaseeq_nbfix.dcd --align_selection "backbone and (resid 20-500)" --atom_selection "backbone and (resid 20-500)" --strategy ddp --batch_size 128 --scheduler reduce -gaccm 1 --train_mode sample --num_epochs 15000 --lr 0.001 --which_model fc --truncate 512 -ckpt epoch=403-step=10100.ckpt
