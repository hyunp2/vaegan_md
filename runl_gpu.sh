#!/bin/bash

JOBNAME="${1/%.namd/}"
GPUMACHINE="${2}"
P="${3}"

echo "$JOBNAME"
if [ -z "$1" ]; then
  echo "Useage: runl NAMD_CONF_FILE GPUMACHINE P"
  exit
fi
if [ ! -f $JOBNAME.namd ]; then
	echo "NAMD input file $JOBNAME.namd does not exist!"
	exit -1
fi

if [ ! -f $JOBNAME.log ]; then
        echo "NAMD output file $JOBNAME.log does not exist!"
        touch $JOBNAME.log 
fi	

echo $GPUMACHINE "is chosen!"
echo $P "CPU cores are chosen!"

NUM_GPUS=$(nvidia-smi --gpu-lists | wc -l)
echo $NUM_GPUS "GPU cores are chosen!"
DEVICES=$(seq -s, 0 $(expr $NUM_GPUS - 1))
echo $DEVICES "devices are chosen!"

qsub -q $GPUMACHINE -N $JOBNAME -j y -o ~/Jobs << EOF
cd "$PWD"
/Projects/dhardy/namd_builds/NAMD_3.0alpha13_Linux-x86_64-multicore-CUDA/namd3 +setcpuaffinity +p${P} +devices $DEVICES $JOBNAME.namd >> $JOBNAME.log
EOF



#namd2 $JOBNAME.namd +setcpuaffinity +p64 +devices '0','1','2','3','4','5','6','7' >& $JOBNAME.log
