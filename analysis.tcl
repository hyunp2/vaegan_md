####PLEASE ADD PSF and PDB AT the VERY FIRST FRAME!####
lappend auto_path /Projects/hyunpark/la1.0
lappend auto_path /Projects/hyunpark/orient
package require Orient
namespace import Orient::orient	

#Energy decomposition (BENZENE, GpA)
#Refer to comp_force

#Number Density (Gumbart, Hu)
proc numberDensity {selection filename} {
	set minZ [lindex [lindex [measure minmax [atomselect top all]] 0] 2]
	set maxZ [lindex [lindex [measure minmax [atomselect top all]] 1] 2]
	set out [open "$filename-temp.dat" "w+"]
	set counterListTotal {}
	for {set i 1} {i < [molinfo top get numframes]} {incr i} {
		animate goto $i
		set counterList {}
		for {set Z [format %.0f $minZ]} {$Z <= [expr {1 + [format %.0f $maxZ]}]} {incr Z} {
			set resList [atomselect top "$selection and z >= $Z and z < [expr {$Z + 1}]"]
			set resListUniq [lsort -unique [$resList get resid]]
			set counter [llength $resListUniq] 
			append counterList $counter " "
			#puts $out [list $Z $counter]
		}
		lappend counterListTotal $counterList
	}
	set counterSum {}
	#close $out
} 

#RMSD of backbone over time (Chipot)
proc rmsdCalc {selection filename} {
	mdff check -rmsd -rmsdfile $filename.dat -rmsdseltext $selection
}

#Angle between two helices (Chipot, Best)
proc angleNormalToMembrane {selection0 selection1 filename} {
	set out [open "$filename.dat" "w+"]
	for {set i 1} {$i < [molinfo top get numframes]} {incr i} {
		animate goto $i
		set resList0 [atomselect top "$selection0"]
		set resList1 [atomselect top "$selection1"]
		set principalZ0 [lindex [Orient::calc_principalaxes $resList0] 2]
		set principalZ1 [lindex [Orient::calc_principalaxes $resList1] 2]
		set angle360 [expr {360 * acos([vecdot $principalZ0 $principalZ1] / ([veclength $principalZ0] * [veclength $principalZ1])) / (2 * 3.141592)}]
		puts $out [list $i $angle360]
	}
	close $out
}
#angleNormalToMembrane "protein and segname PROA" "protein and segname PROB" "qTest"

#SASA (Chipot)
proc sasaCalc {selection filename {molID top}} {
	set out [open "$filename.dat" "w+"]
	for {set i 1} {$i < [molinfo $molID get numframes]} {incr i} {
		animate goto $i
		set resList [atomselect $molID "$selection"]
		set sasa [measure sasa 1.4 $resList -restrict $resList]
		puts $out [list $i $sasa]
	}
	close $out
}

set molID [molinfo top]
#Dist (Chipot)
proc distCalc {atom1 atom2 {filename "bonds"} {molID $molID}} {
	set out [open "$filename.dat" "w+"]
    set bonds [measure bond {$atom1 $atom2} molid $molID frame all]
    puts $out $bonds
    close $out
}

proc angleCalc {atom1 atom2 atom3 {filename "angles"} {molID $molID}} {
	set out [open "$filename.dat" "w+"]
    set angles [measure angle {$atom1 $atom2 $atom3} molid $molID frame all]
    puts $out $angles
    close $out
}

proc angleBetweenCalc {vec1 vec2 {filename "anglesBetween"}} {
	set out [open "$filename.dat" "w+"]
    #set angles [measure angle {$atom1 $atom2 $atom3} molid $molID frame all]
	set vec1 [vector $vec1] #Normalized
	set vec2 [vector $vec2] #Normalized
	set angles [expr acos([vecdot $vec1 $vec2])]
    puts $out $angles
    close $out
}

#Contacts with 3A (Chipot), AVERAGE over TIME!
proc contactsCalc {selection0 selection1 filename} {
	set out [open "$filename-temp.dat" "w+"]
	set contacts [measure contacts 3 [atomselect top $selection0] [atomselect top $selection1]]
	set H1Resnames {}
	set H2Resnames {}
	set H1Resids {}
	set H2Resids {}

	for {set i 0} {$i < [llength [lindex $contacts 0]]} {incr i} {
		set H1Residue [lindex [lindex $contacts 0] $i]
		set H2Residue [lindex [lindex $contacts 1] $i]
		append H1Resnames [[atomselect top "index $H1Residue"] get resname] " "
		append H2Resnames [[atomselect top "index $H2Residue"] get resname] " "
		append H1Resids [[atomselect top "index $H1Residue"] get resid] " "
		append H2Resids [[atomselect top "index $H2Residue"] get resid] " "
	}
	
	for {set i 0} {$i < [llength $H1Resnames]} {incr i} {
		set H1 [lindex $H1Resnames $i]
		set H2 [lindex $H2Resnames $i]
		set H1ID [lindex $H1Resids $i]
		set H2ID [lindex $H2Resids $i]
		puts $out [list $H1 $H1ID $H2 $H2ID]
	}
	close $out

	set fp [open "$filename-temp.dat" r]
	set file_data [read $fp]
	close $fp
	set data [split $file_data "\n"]

	set Huniq [lsort -unique $data]

	set out2 [open "$filename.dat" "w+"]
	for {set i 0} {$i < [llength $Huniq]} {incr i} {
		set counter 0
		for {set j 0} {$j < [expr {[llength $data] -1}]} {incr j} {	
			if {([lindex [lindex $Huniq $i] 0] == [lindex [lindex $data $j] 0]) && ([lindex [lindex $Huniq $i] 1] == [lindex [lindex $data $j] 1]) && ([lindex [lindex $Huniq $i] 2] == [lindex [lindex $data $j] 2]) && ([lindex [lindex $Huniq $i] 3] == [lindex [lindex $data $j] 3])} {
			set counter [expr {$counter + 1}]} else {continue}
		}
		puts $out2 [list "[lindex $Huniq $i]" $counter]
	}
	close $out2
	#less qTest.dat | awk '{print $5}' | xargs  | sed -e 's/\ /+/g' | bc (use for summation check)
}

#APL and Thickness (Gumbart, Hu)
proc aplAndTckCalc {filename} {
	set out [open "$filename.dat" "w+"]
	for {set i 1} {$i < [molinfo top get numframes]} {incr i} {
		animate goto $i
		set numLipidsTop [llength [[atomselect top "name P and z > 0"] get serial]]
		set numLipidsBottom [llength [[atomselect top "name P and z < 0"] get serial]]
		set lengthXYZ [vecsub [lindex [measure minmax [atomselect top all]] 1] [lindex [measure minmax [atomselect top all]] 0]]
		set area [expr {[lindex $lengthXYZ 0] * [lindex $lengthXYZ 1]}]
		set height [lindex $lengthXYZ 2]
		puts $out [list $i [expr {$area/$numLipidsTop}] [expr {$area/$numLipidsBottom}] $height]
	}
	close $out
}

#Distance and Rotation to PMF (
proc distRotPMF {filename} {
	set out [open "$filename.dat" "w+"]
	for {set i 1} {$i < [molinfo top get numframes]} {incr i} {
		animate goto $i
		set dist [vecdist [measure center [atomselect top "segname PROA and name C"] weight mass] [measure center [atomselect top "segname PROB and name C"] weight mass]]
		set rotAngle [measure dihed {178 135 228 664}]
		puts $out [list $dist $rotAngle]
	}	
	close $out
}

#Secondary Structure (Timeline toolkit in VMD)
#Refer to Timeline in VMD

#Entropy and enthalpy contribution
proc deltaSandH {temperature0 temperature1 filename} {
	set fp0 [open "hist0.dat" r]
	set file_data0 [read $fp0]
	close $fp0
	set data0 [split $file_data0 "\n"]
	set fp1 [open "hist1.dat" r]
	set file_data1 [read $fp1]
	close $fp1
	set data1 [split $file_data1 "\n"]

	for {set i 0} {$i < [llength $data0]} {incr i} {
		set xi [lindex [lindex $data0 $i] 0]
		set pmf0 [lindex [lindex $data0 $i] 1]
		set pmf [lindex [lindex $data1 $i] 1]
		set negTS [expr {-()}]

		puts $out [list "[lindex $Huniq $i]" $counter]
	}
	close $out
}





















#proc normalize {input, totalNumber} {
#	set inLine [open $input "r"]
#	set out [open "numDenNorm" "w+"]
#	while{[gets $input line] != -1} {
#		[lindex $line 1]	
#	}	
#}
#close proc


